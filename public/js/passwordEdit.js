  $( document ).ready(function() {
  //variables
  var pass1 = $('.passwordedit1');
  var pass2 = $('.passwordedit2');
  var confirmacion = "Las contraseñas coinciden";
  var negacion = "No coinciden las contraseñas";
 
  //oculto por defecto el elemento span
  var span = $('<span></span>').insertAfter(pass2);

  span.hide();
  //función que comprueba las dos contraseñas
  function coincidePassword(){
  var valor1 = pass1.val();
  var valor2 = pass2.val();
  $(".btnPasswordEdit").attr("disabled", "disabled");
   span.text(negacion).addClass('alert-danger');
  //muestro el span
  span.show().removeClass();
  //condiciones dentro de la función
  if (valor1.length > 0 && valor2.length > 0) {
      if(valor1 != valor2){
  span.text(negacion).addClass('alert-danger');
  $(".btnPasswordEdit").attr("disabled", "disabled");
  }else{
    $(".btnPasswordEdit").removeAttr("disabled");
    span.text(confirmacion).addClass('alert-success');
  }
  }

  if (valor1.length == 0 && valor2.length == 0) {
    span.hide();
     $(".btnPasswordEdit").removeAttr("disabled");
  }



  }
  //ejecuto la función al soltar la tecla
  pass2.keyup(function(){ 
  coincidePassword();
  });

  pass1.keyup(function(){ 
  coincidePassword();
  });
});