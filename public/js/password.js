  $( document ).ready(function() {
  //variables
  var pass1 = $('.password1');
  var pass2 = $('.password2');
  var confirmacion = "Las contraseñas coinciden";
  var negacion = "No coinciden las contraseñas";
  $(".btnPassword").attr("disabled", "disabled");
  //oculto por defecto el elemento span
  var span = $('<span></span>').insertAfter(pass2);

  span.hide();
  //función que comprueba las dos contraseñas
  function coincidePassword(){
  var valor1 = pass1.val();
  var valor2 = pass2.val();
  //muestro el span
  span.show().removeClass();
  //condiciones dentro de la función
  if(valor1 != valor2){
  span.text(negacion).addClass('alert-danger');
  $(".btnPassword").attr("disabled", "disabled");
  }else{
    $(".btnPassword").removeAttr("disabled");
    span.text(confirmacion).addClass('alert-success');
  }


  }
  //ejecuto la función al soltar la tecla
  pass2.keyup(function(){ 
  coincidePassword();
  });


});