<?php

namespace App\Jobs;

use App\Participante;
use App\Role;
use App\Inscripcion;
use App\User;
use Validator;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportCSV implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filePath,$carrera;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($carrera)
    {
        //$this->filePath = $filePath;
        $this->carrera = $carrera;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $participantes = Participante::where('carrera_id',$this->carrera)->get();
        $inscritos= [];
        $users= [];
        $roleUser = [];
        $us=[];
        $fecha = date('Y/m/d');
        $role_api = Role::where('name', 'api')->first();

        foreach ($participantes as $participante) {
            $pass = \Hash::make($participante['num_dorsal']);

            $parti['fecha'] = $fecha;
            $parti['carrera_id'] = $this->carrera;
            $parti['participante_id'] = $participante['id'];
            $inscritos[] = $parti;


            $user['nombre'] = $participante['nombre'].' '.$participante['apellido'];
            $user['email'] = $participante['direccion'];
            $user['num_dorsal'] = $participante['num_dorsal'];
            $user['username'] = $this->carrera.$participante['num_dorsal'];
            $user['id_carrera'] = $this->carrera;
            $user['password'] = $pass;
            $users[] = $user;

        }

        DB::table('inscripcions')->insert($inscritos);
        User::insert($users);


        $users = User::where('id_carrera',$this->carrera)->get();
        foreach ($users as $user) {
            $u['user_id'] = $user['id'];
            $u['role_id'] = $role_api['id'];
            $us[] = $u;
        }
        DB::table('role_user')->insert($us);

        $user_participantes = User::select('participantes.id as id_participante','users.id')
            ->join('participantes','users.num_dorsal','=','participantes.num_dorsal')
            ->where('id_carrera',$this->carrera)->get();

        $userParticipantes = [];

        foreach ($user_participantes as $userParticipante) {
            $userParti['participante_id'] = $userParticipante['id_participante'];
            $userParti['user_id'] = $userParticipante['id'];
            $userParticipantes[] =$userParti;
        }
        

        DB::table('participante_user')->insert($userParticipantes);


        
    }
}
