<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PuntosControl extends Model
{
	protected $table = "puntos_controls";
    protected $fillable = ['titulo','km','carrera_id','tipo_punto','latitud','longitud'];
    public $timestamps = false;

    public function carrera()
    {
        return $this->belongsTo('App\Carrera');
    }

}
