<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Carrera extends Model
{
	protected $table = "carreras";
    protected $fillable = ['nombre','descripcion', 'logo','img_cabecera','url','km_totales','fecha','url_encuesta','csv','visibility'];

    public $timestamps = false;

    public function setUrlAttribute($url){
       $this->attributes['url'] = str_slug($url);

    }


/*    public function setLogoAttribute($logo){
        if (!empty($logo)) {
        $name = Carbon::now()->second.$logo->getClientOriginalName();
        $this->attributes['logo'] = $name;
        \Storage::disk('local')->put($name, \File::get($logo)); 
        }
        
    }*/

/*    public function setImgCabeceraAttribute($img_cabecera){
        if (!empty($img_cabecera)) {
        $name = Carbon::now()->second.$img_cabecera->getClientOriginalName();
        $this->attributes['img_cabecera'] = $name;
        \Storage::disk('local')->put($name, \File::get($img_cabecera)); 
        }
        
    }*/

    public function puntoscontrols()
    {
        return $this->hasMany('App\PuntosControl');
    }

    public function participantes(){
        return $this->hasMany('App\Participante');
    }

    public function patrocinadores(){
        return $this->hasMany('App\Patrocinador');
    }
}
