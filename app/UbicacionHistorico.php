<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UbicacionHistorico extends Model
{
    protected $table = "ubicacion_historico";
    protected $fillable = ['latitud', 'longitud','fecha','hora','participante_id','carrera_id','fecha_competencia'];


    public function getFechaCompetenciaAttribute($value){
  		$fechaUTC = new \DateTime($value, new \DateTimeZone('UTC')); 
  		$fechaUTC->setTimezone(new \DateTimeZone('Europe/Madrid'));
  		$fechaUTC->format('Ymd H:i:s');
  		$fecha = [];
  		foreach ($fechaUTC as $key => $value) {
  			$fecha[] = $value;
  		}
  		$fechaOptimizada = explode(".",$fecha[0]);

  		return $fechaOptimizada[0];

    }
}
