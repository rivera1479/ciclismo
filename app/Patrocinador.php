<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Patrocinador extends Model
{
	protected $table = "patrocinadors";
    protected $fillable = ['nombre', 'url','img','principal','carrera_id'];
    public $timestamps = false;

    public function carrera()
    {
        return $this->belongsTo('App\Carrera');
    }

    public function setImgAttribute($img){
        if (!empty($img)) {
        $name = Carbon::now()->second.$img->getClientOriginalName();
        $this->attributes['img'] = $name;
        \Storage::disk('local')->put($name, \File::get($img)); 
        }
        
    }

    public function setPrincipalAttribute($value){
        
        
        if ($value == 'true') {
            
            $patrocinadorActivo = DB::table('patrocinadors')
            ->where('carrera_id',$this->carrera_id)
            ->where('principal','1')->update(['principal' => 0]);
            $this->attributes['principal'] = 1;
        }
        else{
            $this->attributes['principal'] = 0;
        }

    }
}
