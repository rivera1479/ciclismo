<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distancia extends Model
{
    protected $table = 'distancia_punto_controls';
     protected $fillable = ['punto1','punto2', 'distancia'];
     public $timestamps = false;
}
