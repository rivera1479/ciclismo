<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{

	protected $table = "participantes";
    protected $fillable = ['nombre', 'apellido','dni','num_dorsal','telefono','direccion','carrera_id','perfil'];
    public $timestamps = false;
    public function carrera(){
    	return $this->belongsTo('App\Carrera');
    }

    public function scopeSearchdorsal($query, $numero_dorsal,$id_carrera){
    	return $query->where('num_dorsal','=', "$numero_dorsal")
    		->where('carrera_id','=',"$id_carrera");
    }

    public function scopeSearchtelefono($query, $telefono,$id_carrera){
    	return $query->where('telefono','=', "$telefono")
    	->where('carrera_id','=',"$id_carrera");
    }


    public function users(){
        return $this->belongsToMany('App\User');
    }
}
