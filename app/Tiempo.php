<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiempo extends Model
{
	protected $table = "tiempos";
    protected $fillable = ['tiempo', 'puntos_control_id','participante_id','km','nombre_control'];
    public $timestamps = false;
}
