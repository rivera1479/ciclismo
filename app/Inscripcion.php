<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
	protected $table = "inscripcions";
    protected $fillable = ['fecha', 'carrera_id', 'participante_id'];
    public $timestamps = false;


    public function setFechaAttribute($fecha){
        $now = new \DateTime();
        $this->attributes['fecha'] = $now;
    }

    
    public function participantes(){
    	
    }
}
