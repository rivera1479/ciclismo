<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carrera;
use App\Participante;
use App\PuntosControl;
use Illuminate\Support\Facades\Input;
use Redirect;
use ZipArchive;
//test

use App\UbicacionHistorico;
class FrontController extends Controller
{


    public function index(Request $request){

    	$carreras = Carrera::orderBy('fecha','desc')->paginate(10);

    	return view('index',compact('carreras'));

    }

    public function harverstine($lat1, $long1, $lat2, $long2){ 

        $km = 111.302;  
        $degtorad = 0.01745329;
        $radtodeg = 57.29577951; 
        $dlong = ($long1 - $long2); 
        $dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad)); 
        $dd = acos($dvalue) * $radtodeg; 
        return round(($dd * $km), 2);
    }


    public function distancia(Request $request){
        if($request->ajax()){
            $id_carrera = $request->carrera_id;
            


            $race = Carrera::find($request->carrera_id)->dbplus_first(relation, tuple)();

            $puntos = $race->puntoscontrols()->get();

            $puntosControl = [];
            foreach($puntos as $punto){
                if ($punto['tipo_punto'] == 'Punto de control') {
                    $puntosControl[] = [
                        'lat' =>$punto['latitud'],
                        'lng' => $punto['longitud']];
                    }
                }



                foreach ($puntosControl as $value) {

                    $earth_radius = 6371;
                    $dLat = deg2rad($request->lat - $value['lat']);

                    $dLon = deg2rad($request->lng - $value['lng']);
                    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($value['lat'])) * cos(deg2rad($request->lat)) * sin($dLon/2) * sin($dLon/2);

                    $c = 2 * asin(sqrt($a));
                    $d = $earth_radius * $c;

                }



                $success=array('success'=>true,'mensaje'=>'Tiempo agregado con exito!!','d' =>$d);
                return response()->json($success);

            }
        }

        public function upload(Request $request){

         $file = $request->file('archivo');
         $nombre = $file->getClientOriginalName();
         \Storage::disk('local')->put($nombre,  \File::get($file));


         $zip = new ZipArchive;
         $kmz = public_path().'/img/'.$nombre;
         $res = $zip->open($kmz);
         $path = public_path().'/img/xml/';

         $qtmp=array();
         if ($zip->open($kmz) === true) {
            for($i = 0; $i < $zip->numFiles; $i++) {
                $filename = $zip->getNameIndex($i);
                $xml = simplexml_load_file($path.'/'.$filename);
                $placemarks = $xml->Document->Placemark->LineString->coordinates;

                $cor_d = explode(" ", $placemarks);
                $var = 0;

                foreach($cor_d as $value){
                    $tmp = explode(',',$value);
                    if (count($tmp) >= 2) {
                        $ttmp=trim($tmp[1]);
                        $tmp[1]=trim($tmp[0]);
                        $tmp[0]=$ttmp;

                        $qtmp[]= $tmp[0] . ',' .$tmp[1];
                    }else{
                    }
                }

                foreach ($qtmp as $key) {
                    $var = explode(',',$key);
                    
                    $carrera = PuntosControl::create([
                        'carrera_id' => 4,
                        'titulo' => 'titulo',
                        'tipo_punto' => 'Normal',
                        'latitud' => $var[0],
                        'longitud' => $var[1],
                        'km' => 0
                    ]);                    
                }
                $fileinfo = pathinfo($filename);
                copy("zip://".$path."#".$filename, "/your/new/destination/".$fileinfo['basename']);
            }                  
            $zip->close();                  
        }

        $archivo = $path.'/img/xml/'.$nombre;

        foreach (glob("*.*") as $archivo) {
          echo $archivo."<br />";
      }

      dd($path.'/img/xml/'.$nombre.'/*');
      return "archivo guardado";
  }

  public function buscar(Request $request,$slug,$tipo,$idcarrera){
   $participante= Input::get('participante');
   Redirect::to('pruebas/'.$slug.'/'.$tipo.'/'.$idcarrera.'/'.$participante);
}

public function mapunido(){
    $race = Carrera::where('id','=', 5)->firstOrFail();
    $puntosControl = $race->puntoscontrols()->get();
    $km_distance = 0;
    $arrayKm = [];
    $tiempos = [];
    $todosPuntos = [];
    $tiemposGrafica = [];
    $arrayLatLon = [];
    foreach ($puntosControl as $value) {
        $km_distance = $km_distance + $value['km'];
        if ($value['tipo_punto'] == 'Punto de control') {
            $arrayKm[]=[
                'id' => $value['id'],
                'titulo' => $value['titulo'],
                'km' => $km_distance];
            }
        }

        $participante = Participante::where('id','=', 8289)->firstOrFail();

        $origen = $puntosControl[0]['latitud'].",".$puntosControl[0]['longitud'];
        $ultimoPunto = $puntosControl->last();
        $destino = $ultimoPunto['latitud'].",".$ultimoPunto['longitud'];
        $puntofinal = $puntosControl->count() - 1;
        $waypt = $puntosControl->filter(function($value,$key) use ($puntofinal){
            if ($key > 0 and $key < $puntofinal) {
                return $key;
            }
        });
        $waypts = $waypt->all();
        foreach ($waypts as $key => $value) {
            $tiempo = array_first($tiempos, function($arrayTiempo,$tiempo) use ($value) {
                return $arrayTiempo['id_control'] == $value['id'];
            });
            if ($tiempo['id_control'] == $value['id']) {
                $arrayLatLon[] = [
                    'lat' => $value['latitud'],
                    'lon' => $value['longitud'],
                    'tipo_punto' => $value['tipo_punto'],
                    'titulo' =>$value['titulo'],
                    'km' => $value['km'],
                    'tiempo' => $tiempo['tiempo']
                ];
            }
            else{
                $arrayLatLon[] = [
                    'lat' => $value['latitud'],
                    'lon' => $value['longitud'],
                    'tipo_punto' => $value['tipo_punto'],
                    'titulo' =>$value['titulo'],
                    'km' => $value['km'],
                    'tiempo' => 0
                ];
            }
        }
        $tiempoFinal =  array_first($tiempos, function ($key, $value) use ($puntosControl){
            return $key['id_control'] == $puntosControl->last()->id;
        });
        foreach($puntosControl as $puntos => $control){
            $todosPuntos[] = ['tipo' => $control['tipo_punto'], 'lat' => $control['latitud'],'lon' => $control['longitud']];
        }
        if ($participante == null) {

            $metodo = new MetodoController();
            $latLonUbi = $metodo->ubicacionHistorico($race);

            $mensajebusqueda = 'Este participante no existe';
            return view('mapunido', compact('participante','race','puntos','tiempos','puntosControl','ubicacion','origen','destino','arrayLatLon','todosPuntos','tiempoFinal','tiemposGrafica','latLonUbi','mensajebusqueda'));
        }
        else{
            $ubicacion = UbicacionHistorico::where('participante_id', $participante['id'])->get();

            if ($puntosControl[0]['tipo_punto'] == 'KMZ') {
                return view('mapunido', compact('participante','race','puntos','tiempos','puntosControl','ubicacion','origen','destino','arrayLatLon','todosPuntos','tiempoFinal','tiemposGrafica'));
            }else{

                $puntos = Tiempo::where('participante_id','=',$participante['id'])->get();

                foreach ($puntos as $punto) {
                    for ($i = 0; $i < count($arrayKm) ; $i++){
                        if($arrayKm[$i]['id'] == $punto['puntos_control_id']){
                            $tiempos[]= [
                                'tiempo' => $punto['tiempo'],
                                'distancia' => $arrayKm[$i]['km'],
                                'titulo' => $arrayKm[$i]['titulo'],
                                'id_control' => $punto['puntos_control_id']
                            ];
                            $tiempoActual = $punto['tiempo'];
                            if ($i == 0) {
                                $tiemposGrafica[] =[
                                    'tiempo' => $tiempoActual,
                                    'distancia' => $arrayKm[$i]['km'],
                                    'titulo' => $arrayKm[$i]['titulo'],
                                    'id_control' => $punto['puntos_control_id']
                                ];
                                $tiempoAnterior = $punto['tiempo'];
                            }
                            else{
                                $tiempoRestado = $this->RestarHoras($tiempoAnterior, $tiempoActual);
                                $tiemposGrafica[] =[
                                    'tiempo' => $tiempoRestado,
                                    'distancia' => $arrayKm[$i]['km'],
                                    'titulo' => $arrayKm[$i]['titulo'],
                                    'id_control' => $punto['puntos_control_id']
                                ];
                                $tiempoAnterior = $punto['tiempo'];
                            }
                        }
                    }
                }
                return view('mapunido', compact('participante','race','puntos','tiempos','puntosControl','ubicacion','origen','destino','arrayLatLon','todosPuntos','tiempoFinal','tiemposGrafica'));
            }

        }
    }
}
