<?php

namespace App\Http\Controllers;

use App\Patrocinador;
use App\Carrera;
use Session;
use Redirect;

use Illuminate\Http\Request;

class PatrocinadoresController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    
    public function index($id){
    	$carrera = Carrera::find($id);
    	$patrocinadores = $carrera->patrocinadores()->paginate(5);
    	return view('carreras.patrocinadores.index',compact('carrera','patrocinadores'));

    }

    public function create($id){
    	$carrera = Carrera::find($id);
    	return view('carreras.patrocinadores.create',compact('carrera'));
    }

    public function store(Request $request){
    
    	$patrocinador = new Patrocinador;
    	$patrocinador->nombre = $request->nombre;
    	$patrocinador->img = $request->img;
    	$patrocinador->url = $request->url;
    	if ($request->principal == "true" ) {
    		$patrocinador->principal = $request->principal;	
    	}
    	else{
    		$patrocinador->principal = false;
    	}
    	
    	$patrocinador->carrera_id = $request->carrera;
    	$patrocinador->save();
    	$carrera = $request->carrera;
    	Session::flash('message','Punto creado con exito!');
    	return redirect('/patrocinadores/'.$carrera);
    }

    public function edit($id){
        $patrocinador = Patrocinador::find($id);
        $carrera = $patrocinador->carrera()->first();
        return view('carreras.patrocinadores.edit',compact('patrocinador','carrera'));

    }

    public function update(Request $request){
        $patrocinador = Patrocinador::find($request->patrocinador);
        $patrocinador->fill($request->all());
        $patrocinador->save();
        $carrera= $patrocinador->carrera()->first();
        Session::flash('message','Patrocinador editado correctamente');
        return Redirect::to('/patrocinadores/'.$carrera['id']);

    }

    public function destroy($id){
        $patrocinador = Patrocinador::find($id);
        $carrera = $patrocinador->carrera()->first();
        $patrocinador->delete();
        if(file_exists(public_path('img/'.$patrocinador->img)))
        {
            unlink(public_path('img/'.$patrocinador->img));
        }
        Session::flash('message','Patrocinador eliminado correctamente');
        return Redirect::to('/patrocinadores/'.$carrera['id']);

    }
}
