<?php

namespace App\Http\Controllers;
use App\PuntosControl;
use App\Carrera;
use App\Tiempo;
use App\Participante;
use Session;
use Redirect;
use DB;
use Auth;
use Validator;
use Response;
use ZipArchive;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class PuntosController extends Controller
{

    public function __construct(){
        $this->middleware('admin');
    }


    public function index($id){
    	$carrera = Carrera::find($id);
    	$puntosControl = $carrera->puntoscontrols()->get()->all();
        if (empty($puntosControl)) {

            return view('carreras.puntoscontrol.index',compact('carrera','puntosControl'));
        }else{

          if ($puntosControl[0]['tipo_punto'] == 'KMZ') {

            return view('carreras.puntoscontrol.index',compact('carrera','puntosControl'));
          }
          else{
            $arrayPuntos = [];
            foreach ($puntosControl as $key => $value) {
                $arrayPuntos[] = $value['id'];

            }
            $puntosControl = $carrera->puntoscontrols()->get();
            $origen = $puntosControl[0]['latitud'].",".$puntosControl[0]['longitud'];
            $ultimoPunto = $puntosControl->last();
            $destino = $ultimoPunto['latitud'].",".$ultimoPunto['longitud'];
            $puntofinal = $puntosControl->count() - 1;
            $waypt = $puntosControl->filter(function($value,$key) use ($puntofinal){
                if ($key > 0 and $key < $puntofinal) {
                    return $key;
                }
            });
            $waypts = $waypt->all();
            $arrayLatLon = [];
            foreach ($waypts as $key => $value) {
                $arrayLatLon[] = [
                    'lat' => $value['latitud'],
                    'lon' => $value['longitud'],
                    'tipo_punto' => $value['tipo_punto'],
                    'titulo' => $value['titulo']];
                }
                return view('carreras.puntoscontrol.index',compact('carrera','puntosControl','origen','destino','arrayLatLon'));
              }
            }
          }

        public function create($id){
           $carrera = Carrera::find($id);
           
           return view('carreras.puntoscontrol.create',compact('carrera'));

       }

       public function createkmz($id){
           $carrera = Carrera::find($id);

           
           return view('carreras.puntoscontrol.createkmz',compact('carrera'));

       }

       public function store(Request $request){
          $rules = array(
            'ubicacion' => 'required',
            'inputs' =>'required'
        );

          $mensajes = array(
            'ubicacion.required' => 'La ubicacion es requerida',
            'inputs.required' =>'Los inputs son requeridos',
        );
          $validator = Validator::make(Input::all(), $rules,$mensajes);

          if ($validator->fails()) 

            return Response::json(array('success'=>false,'errors' => $validator->errors()->all()));
        else
        {
            $carrera = Carrera::find($request->carrera_id);
            $puntoscontrol = $carrera->puntoscontrols()->get();
            if (count($puntoscontrol) > 0) {
                PuntosControl::where('carrera_id', $request->carrera_id)->delete();
            }


            $arrayUbicacion = explode("),(",$request->ubicacion);
            $arrayFinal = [];
            foreach ($arrayUbicacion as $key => $value) {
                $arrayLatLon= explode(",",$value);
                $arrayFinal[] = ['lat' => $arrayLatLon[0],
                'lon' => $arrayLatLon[1]];

            }
            $num = 0;
            $nombrePunto = explode('","',json_encode($request->inputs));
            $arrayPunto = [];

            $distancias = explode("km,",$request->distancias);
            $i = 0;
            $km = [];



            foreach ($arrayFinal as $key => $value) {

               $puntos_control = new PuntosControl;
               $puntos_control->carrera_id = $request->carrera_id;
               $puntos_control->titulo = trim($nombrePunto[$num],'["]');
               if ($i == 0) {

                   $puntos_control->km = 0;
               }
               else {

                   $puntos_control->km =  str_replace(",", ".", trim($distancias[$i -1]," km"));
               }
               $puntos_control->latitud = trim($value['lat'],"(");
               $puntos_control->longitud = trim($value['lon'],")");
               $puntos_control->tipo_punto = $request->tipoControl[$num];
               $puntos_control->save();
               $num = $num + 1;
               $i = $i + 1;
               $arrayPunto[] = [$puntos_control];
               $km[] = $puntos_control['km'];

           }
           $km_total = 0;
           foreach ($km as $key => $value) {
             $km_total = $km_total + $value;
         }

         DB::table('carreras')->where('id', $request->carrera_id)->update(['km_totales'=> $km_total]);
         $success=array('success'=>true,'mensaje'=>'Punto creado con Exito!!');
         return response()->json($success);
     }
 }

        public function storekmz(Request $request,$carrera_id){
          $carrera = Carrera::find($request->carrera_id);
          $puntoscontrol = $carrera->puntoscontrols()->get();
          if (count($puntoscontrol) > 0) {
            PuntosControl::where('carrera_id', $request->carrera_id)->delete();
          }
          $file = $request->file('archivo');
          $nombre = $file->getClientOriginalName();
          \Storage::disk('local')->put($nombre,  \File::get($file));
          $zip = new ZipArchive;
          $kmz = public_path().'/img/'.$nombre;
          $res = $zip->open($kmz);
          $path = public_path().'/img/xml/';
          $qtmp=array();
          if ($zip->open($kmz) === true) {
            $zip->extractTo($path);
            for($i = 0; $i < $zip->numFiles; $i++) {
              $filename = $zip->getNameIndex($i);
              $xml = simplexml_load_file($path.'/'.$filename);
              
              // $placemarks = $xml->Document->Placemark->LineString->coordinates;
              if (isset($xml->Document->Placemark->LineString->coordinates)) {
                $placemarks = $xml->Document->Placemark->LineString->coordinates;
              }else{
                $placemarks = $xml->Document->Folder[2]->Placemark->MultiGeometry->LineString->coordinates;
              }
              $cor_d = explode(" ", $placemarks);
              $var = 0;
              foreach($cor_d as $value){
                    $tmp = explode(',',$value);
                    if (count($tmp) >= 2) {
                        $ttmp=trim($tmp[1]);
                        $tmp[1]=trim($tmp[0]);
                        $tmp[0]=$ttmp;
                        $qtmp[]= $tmp[0] . ',' .$tmp[1];
                      }else{
                      }
                    }
                    foreach ($qtmp as $key) {
                      $var = explode(',',$key);
                      PuntosControl::create([
                        'carrera_id' => $carrera_id,
                        'titulo' => 'titulo',
                        'tipo_punto' => 'KMZ',
                        'latitud' => $var[0],
                        'longitud' => $var[1],
                        'km' => 0
                      ]);
                    }
                    $distanciatotal = 0;
                    for ($i=0; $i < count($qtmp) - 1; $i++) {
                      $ubicacion1 =  explode(',',$qtmp[$i]);

                      $lat1 = $ubicacion1[0];
                      $long1 = $ubicacion1[1];
                      $e = $i + 1;
                      $ubicacion2 =  explode(',',$qtmp[$e]);
                      $lat2 = $ubicacion2[0];
                      $long2 = $ubicacion2[1];
                      $distancia = $this->harverstine($lat1, $long1, $lat2, $long2);
                      $distanciatotal = $distanciatotal + $distancia;

                    }
                    dd($distanciatotal);
        }                  
        $zip->close();

    }


    DB::table('carreras')->where('id', $request->carrera_id)->update(['km_totales'=> $distanciatotal]);
    return Redirect::to('/punto/'.$carrera['id']);
 }

    public function harverstine($lat1, $long1, $lat2, $long2){ 

    $km = 111.302;  
    $degtorad = 0.01745329;
    $radtodeg = 57.29577951; 
    $dlong = ($long1 - $long2); 
    $dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad)); 
    $dd = acos($dvalue) * $radtodeg; 
    return round(($dd * $km), 2);
}

 public function edit($id){
   $puntocontrol = PuntosControl::find($id);
   $carrera = $puntocontrol->carrera()->first();
   $tipo= ['Punto de control','Puerto','Punto normal'];

   return view('carreras.puntoscontrol.edit',compact('puntocontrol','carrera','tipo'));
}

public function update(Request $request){
    $id_carrera = $request->carrera;
    $carreras = Carrera::find($id_carrera);

    $puntocontrol = PuntosControl::find($request->puntocontrol);
    $km_prev= $puntocontrol['km'];
    $km_post = $request->km;
    $puntocontrol->fill($request->all());
    $puntocontrol->save();
    $km_prev_total = $carreras['km_totales'] - $km_prev;

    $km = $km_prev_total + $km_post;
    DB::table('carreras')->where('id', $id_carrera)->update(['km_totales'=> $km]);
    $carrera= $puntocontrol->carrera()->first();
    Session::flash('message','Punto de control editado correctamente');
    return Redirect::to('/punto/'.$carrera['id']);
}

public function destroy($id){
    $puntocontrol = PuntosControl::find($id);
    $carrera = $puntocontrol->carrera()->first();
    $km_prev= $puntocontrol['km'];
    $puntocontrol->delete();
    $km_prev_total = $carrera['km_totales'] - $km_prev;
    DB::table('carreras')->where('id', $carrera['id'])->update(['km_totales'=> $km_prev_total]);
    Session::flash('message','Punto de control eliminado correctamente');
    return Redirect::to('/punto/'.$carrera['id']);
}

public function cargarCsv($id){
    $puntocontrol = PuntosControl::find($id);
    $carrera = $puntocontrol->carrera()->first();

    return view('carreras.puntoscontrol.formCsv',compact('puntocontrol','carrera'));
}

public function guardarCsvTiempo(Request $request){

  $carrera_id = $request->carrera_id;
  $campos = explode(",", $request->datajson);            

  $participante = Participante::where('num_dorsal',trim($campos[0],'"'))->first();
  if ($participante['id'] == null) {
    $message = Session::flash('message','No existe el participante');
    $success=array('success'=>false,'message' => 'Error','num_dorsal' => trim($campos[0],'"'));
    return response()->json($success);
  }else{
    $tiempos= Tiempo::create([
      'participante_id' => $participante['id'],
      'puntos_control_id' => $request->punto_control,
      'tiempo' => trim($campos[1],'"')]);

    $message = Session::flash('message','Tiempos cargados satisfactoriamente');
    $success=array('success'=>true,'message' => $message);
    return response()->json($success);
  }
}

}
