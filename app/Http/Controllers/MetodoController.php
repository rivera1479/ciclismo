<?php

namespace App\Http\Controllers;
use DB;
use App\UbicacionHistorico;
use Illuminate\Http\Request;
use App\Carrera;
use Maatwebsite\Excel\Facades\Excel;

class MetodoController extends Controller
{
	function generarClave($length = 6) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function ubicacionHistorico($race){
		$latLonUbi = [];
		$ubicacionRestringido = DB::table('ubicacion_historico')
		->select('participante_id',DB::raw('MAX(id) as lastid'))
		->where('carrera_id',$race['id'])
		->groupBy('participante_id')->get();

		foreach ($ubicacionRestringido as $ubicacion => $restringido) {
			$latLonUbi[] = UbicacionHistorico::select('ubicacion_historico.latitud','ubicacion_historico.longitud','ubicacion_historico.id','ubicacion_historico.participante_id','ubicacion_historico.fecha_competencia','participantes.nombre','participantes.num_dorsal')
			->join('participantes','ubicacion_historico.participante_id','=','participantes.id')
			->where('ubicacion_historico.id','=',$restringido->lastid)
			->get();
		}
		
		return $latLonUbi;
	}

	function buscarParticipante(){
		$participante = Participante::searchdorsal($request->participante,$race['id'])->first();
	}

	    // Return a schedule CSV
	public function fetchCsv($request)
	{
		$path = $request->file('csv')->getRealPath();
		$data = Excel::load($path)->get();

		return $data;
	}

	public function campoRequired($campo,$nombre,$cant){
		if ($campo === null) {
			return ['status'=>true,'message'=>'Campo '.$nombre.' requerido en la fila '.$cant.' del csv'];
		}else{
			return false;
		}
	}
}
