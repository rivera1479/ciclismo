<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Participante;
use App\UbicacionHistorico;
use App\Carrera;
class CarreraController extends Controller
{
    function index(){
      //$carrera = Carrera::all();
    	//$carrera = Carrera::all()->where('visibility', 1);
      $carrera = Carrera::where('visibility',1)->get();
      	if (count($carrera) > 0) {
            return response()->json([
                'success'=>true,
                'carreras' => $carrera
            ],200);
      	}
      	else{
            return response()->json([
                'success'=>false,
                'errors'=>'No hay carrera registradas'
            ],400);
      	}

	 }

	 function ingresarCarrera(Request $request){
	 	$carrera = Carrera::find($request->id);
	 	$patrocinadores = $carrera->patrocinadores()->get();
            return response()->json([
                'success'=>true,
                'carreras' => $carrera,
                'patrocinadores' => $patrocinadores
            ],200);

	 }
    
}
