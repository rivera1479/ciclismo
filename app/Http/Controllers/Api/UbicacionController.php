<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Participante;
use App\UbicacionHistorico;
use App\Carrera;
class UbicacionController extends Controller
{
    public function store(Request $request){
        $user = Auth::user();

        //validar que la carrera este iniciada
        $carrera = Carrera::find($user->id_carrera);

        if($carrera->status== 0){
            return response()->json([
                'success'=>false,
                'errors'=>'La carrera debe estar iniciada para poder guardar las ubicaciones'
            ],400);
        }

		$rules=[
			'latitud'=>'required',
			'longitud'=>'required',
			'fecha' => 'required',
			'hora' => 'required',
            'fecha_competencia' => 'required'
		];

		$mensajes=[
			'latitud.required' => 'La Latitud es requerida',
			'longitud.required' => 'La Longitud es requerida',
			'fecha.required' => 'La Fecha es requerida',
			'hora.required' => 'La hora es requerida',
                        'fecha_competencia.required' => 'La Fecha  y la hora son  requeridas',
		];

		$validator = \Validator::make($request->all(), $rules,$mensajes);
        if ($validator->fails()) {
            return response()->json([
            	'success'=>false,
            	'errors'=>$validator->errors()->all()
            ],400);
        }
        
        $Participante=Participante::where('num_dorsal',$user->num_dorsal)
        ->where('carrera_id',$user->id_carrera)->first(); 
        $UbicacionHistorico= New UbicacionHistorico();
        $UbicacionHistorico->latitud=$request->latitud;
        $UbicacionHistorico->longitud=$request->longitud;
        $UbicacionHistorico->fecha=$request->fecha;
        $UbicacionHistorico->hora=$request->hora;
        $UbicacionHistorico->fecha_competencia=$request->fecha_competencia;
        $UbicacionHistorico->carrera_id=$user->id_carrera;
        $UbicacionHistorico->participante_id=$Participante->id;
        $UbicacionHistorico->save();

        return  response()->json([
        	'success'=>true,
        	'msj'=>'Ubicacion Guardada Con Exito',
        	'ubicacion'=>$UbicacionHistorico
        ],200);
    }
}
