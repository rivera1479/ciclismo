<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Carrera;
use Session;



class AuthController extends Controller
{
    public function login(Request $request)
	{
		$rules=[
			'username'=>'required',
			'password'=>'required',
			'id_carrera' => 'required'
		];

		$mensajes=[
			'username.required' => 'El nombre de usuario es requerido',
			'password.required' => 'La contraseña es requerido',
			'id_carrera.required' => 'El id de la carrera es requerido'
		];

		$validator = \Validator::make($request->all(), $rules,$mensajes);
        if ($validator->fails()) {
            return response()->json([
            	'success'=>false,
            	'errors'=>$validator->errors()->all()
            ],400);
        }
		
		//verificar que id de la carrera existe
		$carrera = Carrera::find($request->id_carrera);
		$request["username"] = $request->id_carrera.$request->username; 

		if($carrera==null){
			return response()->json([
				'success'=>false,
	            'error' => ['Debe introducir un id de la carrera válido']
	        ], 400);
		}		

	    if (Auth::attempt($request->only('username', 'password'))) {
	        $user = Auth::user();
	        $token =  $user->createToken('ciclismo')->accessToken;

	        //insertar el id de la carrera
	        $userupdate=User::find($user->id);
	        $userupdate->id_carrera=$request->id_carrera;
	        $userupdate->save();
	        $patrocinadores = $carrera->patrocinadores()->get();

	        return response()->json([
	        	'success'=>true,
	            'token' => $token,
	            'user' => $userupdate,
	            'carrera' => $carrera,
	            'patrocinadores' => $patrocinadores
	        ], 200);
	    } else {
	        return response()->json([
	        	'success'=>false,
	        	'error' => ['Usuario y/o Contraseña no Válidos']
	        ], 401);
	    }
	}

	public function logoutApi(Request $request){
		$var = $request->all();
		if (Auth::check()){
			Auth::user()->AauthAcessToken()->delete();
			return response()->json([
	        	'success'=>true,
	        	'var' => $var,
	        	'mensaje' => ['se elimino']
	        ], 200);
		}else{
			return response()->json([
	        	'success'=>false,
	        	'var' => $var,
	        	'error' => ['No se elimino']
	        ], 401);
		}
	}


	public function profile()
	{
    	$user = Auth::user();
    	return response()->json(compact('user'), 200);
	}


}
