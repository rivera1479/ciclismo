<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Participante;
use App\Role;
use App\Inscripcion;
use App\User;
use App\Jobs\ImportCSV;
use App\Http\Controllers\MetodoController;
use Validator;
use Response;

class CsvController extends Controller
{
	public function import(Request $request){
		$carrera = $request->id_carrera;
		$metodos = new MetodoController();
		User::where('id_carrera',$carrera)->delete();
		Participante::where('carrera_id',$carrera)->delete();

		$ver = $request->validate([
			'csv' => 'required|mimes:csv,txt'
		]);

        $data =  $metodos->fetchCsv($request); // Fetch the CSV data 
        $headerRow = $data->first()->keys()->toArray(); // Fetch the header row
        $validate = $this->validateHeaderRow($headerRow); // Filter it through our validation 

        $errores = '';
        // If our header row passed validation 
        if( $validate == true )
        {
            // Load $data into array if > 0 
        	if($data->count()){
        		$arr = $this->loadCsvIntoArray($data, $request,$carrera);

                // Write to the database 
        		if(!empty($arr['validos'])){
        			Participante::insert($arr['validos']);
        		}
        		if (!empty($arr['invalidos'])) {
        			$errores = $arr['invalidos'];
        		}
        	}

        }

        ImportCSV::dispatch($carrera);


        // Return our import finished message
        $message = $this->returnMessage($validate, $request,$errores);
        

        return $message;
    }

    public function validateHeaderRow($headerRow)
    {
    	$validate = false;

    	if( $headerRow[0] == 'num_dorsal'
    		&& $headerRow[1] == 'nombre' 
    		&& $headerRow[2] == 'apellido'
    		&& $headerRow[3] == 'direccion'
    		&& $headerRow[4] == 'telefono'
    		&& $headerRow[5] == 'dni' )

    	{
    		$validate = true;
    	} 

    	return $validate;
    }

    // Load the import .CSV data into an array
    public function loadCsvIntoArray($data, $request,$carrera)
    {
    	$errores = [];
    	$arr = [];
    	$cant = 2;
    	foreach ($data as $key => $value) {
    		$validador = $this->validateRow($value,$cant);
    		if (empty($validador)) {
    			$arr[] = [
    				'num_dorsal' => $value->num_dorsal,
    				'nombre' => $value->nombre,
    				'apellido' => $value->apellido,
    				'direccion' => $value->direccion,
    				'telefono' => $value->telefono,
    				'dni' => $value->dni,
    				'carrera_id' => $carrera
    			];                
    		}else{
    			foreach ($validador as $message) {
    				$errores[] = $message;
    			}
    		}
    		$cant=$cant+1;      
    	}

    	$arrayFinal = ['validos' =>$arr,'invalidos' =>$errores];

    	return $arrayFinal;
    }

    public function validateRow($value,$cant){
    	$metodos = new MetodoController();

    	$errores = [];
    	$validacion = ['num_dorsal' => $metodos->campoRequired($value->num_dorsal,'numero de dorsal',$cant),
    	'nombre' => $metodos->campoRequired($value->nombre,'nombre',$cant),
    	'apellido' => $metodos->campoRequired($value->apellido,'apellido',$cant),
    	'dni' => $metodos->campoRequired($value->dni,'dni',$cant),
    	'direccion' => $metodos->campoRequired($value->direccion,'dirección de correo electrónico',$cant),
    	'telefono' => $metodos->campoRequired($value->telefono,'telefono',$cant)];
    	foreach ($validacion as $key => $value) {
    		if (is_array($value)) {
    			$errores[] = $value['message'];
    		}

    	}


    	return $errores;
    }

    public function returnMessage($validate, $request,$errores)
    {
    	if( $validate == true )
    	{
    		if (!empty($errores)) {
    			return back()->with(['success'=> 'Se cargaron una cantidad de registros!','errores' => $errores]);
    		}else{
    			return back()->with('success', 'Se cargaron todos los registros!');

    		}

    	} else {
    		return back()->with('danger', 'Tu encabezado debe tener el siguiente formato: num_dorsal,nombre,apellido,direccion,telefono,dni en estricto orden, no se pudo cargar el archivo');
    	}
    }

    public function descargarEjemplo(){
    	return Response::download(public_path('ejemplo.csv'));
    }
}
