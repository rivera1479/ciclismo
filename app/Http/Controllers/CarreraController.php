<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carrera;
use App\Participante;
use App\Tiempo;
use App\PuntosControl;
use App\User;
use App\Role;
use App\Inscripcion;
use App\Distancia;
use App\UbicacionHistorico;
use App\Http\Controllers\MetodoController;
use Auth;
use Validator;
use Session;
use Response;
use Redirect;
use DB;
use App\Slim;
use DateTime;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

class CarreraController extends Controller{

    public function __construct(){
        $this->middleware('userweb',['only' => ['puntosIndex','anadirPunto']]);
        $this->middleware('admin',['except' => ['show','buscarParticipante','puntosIndex','anadirPunto','map','distancia','buscar']]);
    }

    public function index(){
        $carreras=Carrera::orderBy('fecha','desc')->paginate(10);
        return view('carreras.index',compact('carreras'));
    }

    public function distancia(Request $request){
        if($request->ajax()){
            $race = Carrera::find($request->carrera_id)->first();
            $puntos = $race->puntoscontrols()->get();
            $puntosControl = [];
            foreach($puntos as $punto){
                if ($punto['tipo_punto'] == 'Punto de control') {
                    $puntosControl[] = [
                        'lat' =>$punto['latitud'],
                        'lng' => $punto['longitud']];
                }
            }
            $px = $request->px;

          
                $earth_radius = 6371;
                $dLat = deg2rad($request->lat - $puntosControl[$px]['lat']);
                $dLon = deg2rad($request->lng - $puntosControl[$px]['lng']);
                $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($puntosControl[$px]['lat'])) * cos(deg2rad($request->lat)) * sin($dLon/2) * sin($dLon/2);
                $c = 2 * asin(sqrt($a));
                $d = $earth_radius * $c;
                $distancia =  explode('.', $d);
                if ($distancia[0] == 0) {
                    $px = $px + 1;
                }
                $success=array('success'=>true,'px' =>$px,'distancia' =>$distancia[0]);
                return response()->json($success);

        }

    }

    public function create(){
        return view('carreras.create');
    }

    public function store(Request $request){


        $carrera = new Carrera;
        $carrera->nombre = $request->nombre;
        $carrera->url = $request->nombre;
        $carrera->descripcion = $request->descripcion;
        $carrera->fecha = $request->fecha;
        $carrera->visibility = $request->visibility;
        if ($request->logo){
            $image = head(Slim::getImages('logo'));

            if ( isset($image['output']['data']) ){
                $name = $image['output']['name'];
                $data = $image['output']['data'];
                $path = base_path() . '/public/img/';
                $file = Slim::saveFile($data, $name, $path);
                $imagePath = asset('img/'.$file['name']);
                $carrera->logo = $file['name'];
            }
        }



        if ($request->img_cabecera) {
            $image = head(Slim::getImages('img_cabecera'));
            dd($image);
            if ( isset($image['output']['data']) ){
                $name = $image['output']['name'];
                $data = $image['output']['data'];
                $path = base_path() . '/public/img/';
                $file = Slim::saveFile($data, $name, $path);
                $imagePath = asset('img/'.$file['name']);

                $carrera->img_cabecera = $file['name'];
            }
        }

        if ($request->url_encuesta == !null) {
            $carrera->url_encuesta = $request->url_encuesta;
        }

        $carrera->save();
        Session::flash('message','Carrera creada satisfactoriamente');
        return Redirect::to('/carreras');
    }

    public function edit($id){
        $carrera = Carrera::find($id);
        return view('carreras.edit',compact('carrera'));
    }

    public function update(Request $request){
        $carrera = Carrera::find($request->carreraID);
        $carrera->nombre = $request->nombre;
        $carrera->url = $request->nombre;
        $carrera->descripcion = $request->descripcion;
        $carrera->fecha = $request->fecha;
        $carrera->visibility = $request->visibility;
        if ($request->logo){
            $image = head(Slim::getImages('logo'));
            if ( isset($image['output']['data']) ){
                $name = $image['output']['name'];
                $data = $image['output']['data'];
                $path = base_path() . '/public/img/';
                $file = Slim::saveFile($data, $name, $path);
                $imagePath = asset('img/'.$file['name']);
                $carrera->logo = $file['name'];
            }
        }

        if ($request->img_cabecera) {
            $image = head(Slim::getImages('img_cabecera'));
            if ( isset($image['output']['data']) ){
                $name = $image['output']['name'];
                $data = $image['output']['data'];
                $path = base_path() . '/public/img/';
                $file = Slim::saveFile($data, $name, $path);
                $imagePath = asset('img/'.$file['name']);
                $carrera->img_cabecera = $file['name'];
            }
        }
        $carrera->url_encuesta = $request->url_encuesta;
        $carrera->save();

        Session::flash('message','La carrera fue editada correctamente');
        return Redirect::to('/carreras');

    }

    public function destroy($id){
    	$carrera = Carrera::find($id);
        $carrera->delete();
        if(file_exists(public_path('img/'.$carrera->logo)))
        {
            unlink(public_path('img/'.$carrera->logo));
        }
        if(file_exists(public_path('img/'.$carrera->img_cabecera)))
        {
            unlink(public_path('img/'.$carrera->img_cabecera));
        }        
        Session::flash('message','La carrera fue eliminada correctamente');
        return Redirect::to('/carreras');
    }

    public function show($slug){
        $metodo = new MetodoController();
        $race = Carrera::where('url','=', $slug)->firstOrFail();
        $puntosControl = $race->puntoscontrols()->get();
        

        if ($puntosControl[0]['tipo_punto'] == 'KMZ') {
            $latLonUbi = $metodo->ubicacionHistorico($race);
            return \View::make('carreras.detalles', compact('race','puntosControl','latLonUbi'));
        }
        else{
            $origen = $puntosControl[0]['latitud'].",".$puntosControl[0]['longitud'];
            $ultimoPunto = $puntosControl->last();
            $destino = $ultimoPunto['latitud'].",".$ultimoPunto['longitud'];
            $puntofinal = $puntosControl->count() - 1;
            $latLonUbi = [];
            $waypt = $puntosControl->filter(function($value,$key) use ($puntofinal){
                if ($key > 0 and $key < $puntofinal) {
                    return $key;
                }
            });
            

            $latLonUbi = $metodo->ubicacionHistorico($race);

            $waypts = $waypt->all();
            $arrayLatLon = [];
            foreach ($waypts as $key => $value) {
                $arrayLatLon[] = [
                    'lat' => $value['latitud'],
                    'lon' => $value['longitud'],
                    'tipo_punto' => $value['tipo_punto'],
                    'titulo' =>$value['titulo'],
                    'km' => $value['km']];
            }

            $todosPuntos = [];
            foreach($puntosControl as $puntos => $control){
                $todosPuntos[] = [
                    'tipo' => $control['tipo_punto'],
                    'lat' => $control['latitud'],
                    'lon' => $control['longitud']];
            }

            return \View::make('carreras.detalles', compact('race','origen','destino','arrayLatLon','todosPuntos','latLonUbi','puntosControl'));

        }

    }

    public function puntosIndex(){
        $user = Auth::user();
        $num_dorsal = $user['num_dorsal'];
        $participante = Participante::where('num_dorsal',$num_dorsal)->first();
        $carreras = $participante->carrera()->get();
        $carrera = Carrera::find($carreras[0]['id']);
        $puntos=$carrera->puntoscontrols()->get();
        $tiempos = Tiempo::where('participante_id',$participante['id'])->get();
        return view('puntos.index',compact('puntos','carreras','participante','tiempos'));
    }

    public function anadirPunto(Request $request){
        $rules = array(
            'tiempo' => 'required|date_format:H:i:s',
        );

        $mensajes = array(
            'tiempo.required' => 'La hora es requerida',
            'tiempo.date_format' => 'Debe ser en formato 12:00:00'
        );

        $validator = Validator::make(Input::all(), $rules,$mensajes);

        if($validator->fails())
            return Response::json(array('success'=>false,'errors' => $validator->errors()->all()));
        else{
            $puntos = PuntosControl::find($request->puntos_control_id);
            $id_carrera = $puntos['carrera_id'];
            $carrera = Carrera::find($id_carrera);
            $countPuntos = $carrera->puntoscontrols()->get();
            $num = count($countPuntos);
            $tiempo = new Tiempo;
            $tiempo->tiempo = $request->tiempo;
            $tiempo->puntos_control_id = $request->puntos_control_id;
            $tiempo->participante_id = $request->participante_id;
            $tiempo->nombre_control = $request->nombre_control;
            $tiempo->km = $request->km;
            $tiempo->save();
            $success=array('success'=>true,'mensaje'=>'Tiempo agregado con exito!!','num' =>$num);
            return response()->json($success);
        }
    }

    public function buscar(Request $request){
        if ($request->ajax()) {
            $race = Carrera::where('url','=', $request->url)->firstOrFail();

            $participante = Participante::searchdorsal($request->num,$race['id'])->first();
            if ($participante == null) {
                $participante = Participante::searchtelefono($request->num,$race['id'])->first();
            }

            if ($participante == null) {

                $mensaje = 'No hay participante que coincidan.';
                $success=array('success'=>false,'error' =>$mensaje);
                return response()->json($success);
            }else{
                $success=array('success'=>true,'url' =>$request->url,'num_dorsal' => $participante['num_dorsal']);
                return response()->json($success);
            }
        }
    }

    public function buscarParticipante(Request $request,$slug,$num){
        $race = Carrera::where('url','=', $slug)->firstOrFail();
        $puntosControl = $race->puntoscontrols()->get();
        $km_distance = 0;
        $arrayKm = [];
        $tiempos = [];
        $todosPuntos = [];
        $tiemposGrafica = [];
        $arrayLatLon = [];
        foreach ($puntosControl as $value) {
            $km_distance = $km_distance + $value['km'];
            if ($value['tipo_punto'] == 'Punto de control') {
                $arrayKm[]=[
                    'id' => $value['id'],
                    'titulo' => $value['titulo'],
                    'km' => $km_distance];
            }
        }

        $participante = Participante::searchdorsal($num,$race['id'])->first();
        if ($participante == null) {
            $participante = Participante::searchtelefono($num,$race['id'])->first();
        }

        $origen = $puntosControl[0]['latitud'].",".$puntosControl[0]['longitud'];
        $ultimoPunto = $puntosControl->last();
        $destino = $ultimoPunto['latitud'].",".$ultimoPunto['longitud'];
        $puntofinal = $puntosControl->count() - 1;
        $waypt = $puntosControl->filter(function($value,$key) use ($puntofinal){
            if ($key > 0 and $key < $puntofinal) {
                return $key;
            }
        });
        $waypts = $waypt->all();
        foreach ($waypts as $key => $value) {
            $tiempo = array_first($tiempos, function($arrayTiempo,$tiempo) use ($value) {
                return $arrayTiempo['id_control'] == $value['id'];
            });
            if ($tiempo['id_control'] == $value['id']) {
                $arrayLatLon[] = [
                    'lat' => $value['latitud'],
                    'lon' => $value['longitud'],
                    'tipo_punto' => $value['tipo_punto'],
                    'titulo' =>$value['titulo'],
                    'km' => $value['km'],
                    'tiempo' => $tiempo['tiempo']
                ];
            }
            else{
                $arrayLatLon[] = [
                    'lat' => $value['latitud'],
                    'lon' => $value['longitud'],
                    'tipo_punto' => $value['tipo_punto'],
                    'titulo' =>$value['titulo'],
                    'km' => $value['km'],
                    'tiempo' => 0
                ];
            }
        }
        $tiempoFinal =  array_first($tiempos, function ($key, $value) use ($puntosControl){
            return $key['id_control'] == $puntosControl->last()->id;
        });
        foreach($puntosControl as $puntos => $control){
            $todosPuntos[] = ['tipo' => $control['tipo_punto'], 'lat' => $control['latitud'],'lon' => $control['longitud']];
        }
        if ($participante == null) {

            $metodo = new MetodoController();
            $latLonUbi = $metodo->ubicacionHistorico($race);

            $mensajebusqueda = 'Este participante no existe';
            return view('carreras.detalles', compact('participante','race','puntos','tiempos','puntosControl','ubicacion','origen','destino','arrayLatLon','todosPuntos','tiempoFinal','tiemposGrafica','latLonUbi','mensajebusqueda'));
        }
        else{
            $ubicacion = UbicacionHistorico::where('participante_id', $participante['id'])->latest()->take(3)->get();

            if ($puntosControl[0]['tipo_punto'] == 'KMZ') {
                return view('carreras.detalles', compact('participante','race','puntos','tiempos','puntosControl','ubicacion','origen','destino','arrayLatLon','todosPuntos','tiempoFinal','tiemposGrafica'));
            }else{

            $puntos = Tiempo::where('participante_id','=',$participante['id'])->get();
            
            foreach ($puntos as $punto) {
                for ($i = 0; $i < count($arrayKm) ; $i++){
                    if($arrayKm[$i]['id'] == $punto['puntos_control_id']){
                        $tiempos[]= [
                            'tiempo' => $punto['tiempo'],
                            'distancia' => $arrayKm[$i]['km'],
                            'titulo' => $arrayKm[$i]['titulo'],
                            'id_control' => $punto['puntos_control_id']
                        ];
                        $tiempoActual = $punto['tiempo'];
                        if ($i == 0) {
                            $tiemposGrafica[] =[
                                'tiempo' => $tiempoActual,
                                'distancia' => $arrayKm[$i]['km'],
                                'titulo' => $arrayKm[$i]['titulo'],
                                'id_control' => $punto['puntos_control_id']
                            ];
                            $tiempoAnterior = $punto['tiempo'];
                        }
                        else{
                            $tiempoRestado = $this->RestarHoras($tiempoAnterior, $tiempoActual);
                            $tiemposGrafica[] =[
                                'tiempo' => $tiempoRestado,
                                'distancia' => $arrayKm[$i]['km'],
                                'titulo' => $arrayKm[$i]['titulo'],
                                'id_control' => $punto['puntos_control_id']
                            ];
                            $tiempoAnterior = $punto['tiempo'];
                        }
                    }
                }
            }
            return view('carreras.detalles', compact('participante','race','puntos','tiempos','puntosControl','ubicacion','origen','destino','arrayLatLon','todosPuntos','tiempoFinal','tiemposGrafica'));


            }

        }
    }

    function RestarHoras($horaini,$horafin){
        $f1 = new DateTime($horaini);
        $f2 = new DateTime($horafin);
        $d = $f1->diff($f2);
        return $d->format('%H:%I:%S');
    }

    function sumahoras($hora1,$hora2){
        $hora1=explode(":",$hora1);
        $hora2=explode(":",$hora2);
        $horas=(int)$hora1[0]+(int)$hora2[0];
        $minutos=(int)$hora1[1]+(int)$hora2[1];
        $segundos=(int)$hora1[2]+(int)$hora2[2];
        $horas+=(int)($minutos/60);
        $minutos=(int)($minutos%60)+(int)($segundos/60);
        $segundos=(int)($segundos%60);
        return (intval($horas)<10?'0'.intval($horas):intval($horas)).':'.($minutos<10?'0'.$minutos:$minutos).':'.($segundos<10?'0'.$segundos:$segundos);
    }

    public function tiempos(Request $request,$id){
        $participante = Participante::find($id);
        $puntos = Tiempo::where('participante_id','=',$id)->get();
        return view('puntos.detalles',compact('puntos','participante'));
    }

    public function formimport($id){
        $carrera = Carrera::find($id);
        return view('carreras.import',compact('carrera'));
    }


    public function export(Request $request){
        $carrera =Carrera::find($request->id_carrera);
        $file= public_path(). "/upload/csv/".$carrera->csv;
        $headers = array(
            'Content-Type: application/csv',
        );
        return Response::download($file, $carrera->csv, $headers);
    }

    function status(Request $request, $status){
        $carrera = Carrera::find($request->carrera_id);
        if ($status == 1) {
            $carrera->status = 0;
        }
        else{
            $carrera->status = 1;
        }
        $carrera->save();
        $new_status = $carrera['status'];
        $success=array('success'=>true,'mensaje'=>'Tiempo agregado con exito!!', 'new_status' => $new_status, 'carrera'=>$carrera);
        return response()->json($success);
    }

    public function guardar(Request $request){
        $rules = array(
            'ubicacion' => 'required',
            'inputs' =>'required'
        );
        $mensajes = array(
            'ubicacion.required' => 'La ubicacion es requerida',
            'inputs.required' =>'Los inputs son requeridos',
        );
        $validator = Validator::make(Input::all(), $rules,$mensajes);
        if ($validator->fails()) 
            return Response::json(array('success'=>false,'errors' => $validator->errors()->all()));
        else{
            $arrayUbicacion = explode("),(",$request->ubicacion);
            $arrayFinal = [];
            foreach ($arrayUbicacion as $key => $value) {
                $arrayLatLon= explode(",",$value);
                $arrayFinal[] = ['lat' => $arrayLatLon[0],
                'lon' => $arrayLatLon[1]];
            }
            $num = 0;
            $nombrePunto = explode('","',json_encode($request->inputs));
            $arrayPunto = [];
            foreach ($arrayFinal as $key => $value) {
                $puntos_control = new PuntosControl;
                $puntos_control->carrera_id = 1;
                $puntos_control->titulo = trim($nombrePunto[$num],'["]');
                $puntos_control->km = 12;
                $puntos_control->latitud = trim($value['lat'],"(");
                $puntos_control->longitud = trim($value['lon'],")");
                $puntos_control->save();
                $num = $num + 1;
                $arrayPunto[] = [$puntos_control];
            }
            $distancias = explode("km,",$request->distancias);
            for ($i=1; $i < count($arrayPunto)  ; $i++) { 
                $distancia = new Distancia;
                $distancia->punto1 = $arrayPunto[$i-1][0]['id'];
                $distancia->punto2 = $arrayPunto[$i][0]['id'];
                $distancia->distancia = trim($distancias[$i-1]," km");
                $distancia->save();
            }
            $success=array('success'=>true,'mensaje'=>'Punto creado con Exito!!');
            return response()->json($success);
        }
    }

}