<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Rol;
use Session;
use Redirect;
use Illuminate\Http\Request;
use Auth;

class UsuarioController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    public function index(){
        $role = Role::findOrFail(1);
        $usuarios = $role->users;
        return view('usuarios.index',compact('usuarios'));
    }

    public function create(){
    	return view('usuarios.create');
    }

    public function store(Request $request){
    	$role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
    	if($request->rol == 1){
    		$usuario = new User;
    		$usuario->nombre = $request->nombre_admin;
    		$usuario->email = $request->email;
    		$usuario->password =  $request->password_admin;
    		$usuario->save();
    		$usuario->roles()->attach($role_admin);
    	}
    	else{
    		$usuario = new User;
    		$usuario->nombre = $request->nombre_user;
    		$usuario->num_dorsal = $request->num_dorsal;
    		$usuario->password = $request->password_user;
    		$usuario->save();
    		$usuario->roles()->attach($role_user);
    	}
    	Session::flash('message','Usuario agregado con extito');
        return Redirect::to('/usuarios');
    }

    public function edit($id){
    	$usuario = User::find($id);
    	$rol = $usuario->roles()->first();
    	$rol = $rol['id'];
        $roles = Role::all();
    	return view('usuarios.edit',compact('usuario','rol','roles'));
    }

    public function update(Request $request){
        $usuario = User::find($request->id);
        $usuario->fill($request->all());
        $usuario->save();
        Session::flash('message','Usuario editado con extito');
        return Redirect::to('/usuarios');
    	
    }

    public function destroy($id){
        $usuario = User::find($id);
        $usuario->delete();
        Session::flash('message','El usuario fue eliminado correctamente');
        return Redirect::to('/usuarios');
    	
    }
}
