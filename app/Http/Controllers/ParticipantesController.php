<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Carrera;
use App\Participante;
use App\Inscripcion;
use App\Tiempo;
use App\UbicacionHistorico;
use App\PuntosControl;
use App\User;
use App\Role;
use App\Http\Controllers\MetodoController;
use Auth;
use Validator;
use Session;
use Response;
use Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class ParticipantesController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    public function index(Request $request, $id){
        $carrera = Carrera::find($id);
        if ($request->num_dorsal != null ){
            $participantes = $carrera->participantes()->where('num_dorsal',$request->num_dorsal)->orderBy('num_dorsal', 'asc')->paginate(10);
            return view('carreras.participantes.index',compact('participantes','carrera'));
        }else{
            $participantes = $carrera->participantes()->orderBy('num_dorsal', 'asc')->paginate(10);
            return view('carreras.participantes.index',compact('participantes','carrera'));

        }

    }

    public function create($id){
    	$carrera = Carrera::find($id);
    	return view('carreras.participantes.create',compact('carrera'));
    }

    public function store(Request $request){
        $carrera = $request->carrera;
        $role_api = Role::where('name', 'api')->first();
        $participante = new Participante;
        $participante->nombre = $request->nombre;
        $participante->apellido = $request->apellido;
        $participante->dni = $request->dni;
        $participante->num_dorsal = $request->num_dorsal;
        $participante->telefono = $request->telefono;
        $participante->direccion = $request->direccion;
        $participante->carrera_id = $request->carrera;
        $participante->save();

        //Inscripcion del participante
        $inscripcion = Inscripcion::create([
            'fecha' => '',
            'carrera_id'=>$carrera,
            'participante_id'=> $participante['id']]);

        // usuario api participante
        $userp= User::create([
            'nombre' => $participante['nombre']." ".$participante['apellido'],
            'num_dorsal' => $participante['num_dorsal'],
            'username' => $carrera.$participante['num_dorsal'],
            'password' => $request->password,
            'id_carrera' => $carrera,
        ]);
        $userp->roles()->attach($role_api);
        $participante->users()->attach($userp['id']);

        Session::flash('message','Participante agregado con exito!');
        return redirect('/ciclistas/'.$carrera);

    }

    public function edit($id){
        $participante = Participante::find($id);
        $carrera = $participante->carrera()->first();
        return view('carreras.participantes.edit',compact('participante','carrera'));

    }

    public function update(Request $request){
        $participante = Participante::find($request->participante);
        $participante->fill($request->all());
        $participante->save();
        $carrera= $participante->carrera()->first();
        Session::flash('message','Participante editado correctamente');
        return Redirect::to('/ciclistas/'.$carrera['id']);

    }

    public function destroy($id){
        $participante = Participante::find($id);
        $carrera = $participante->carrera()->first();
        $user = $participante->users()->get();
        $user[0]->delete();
        $participante->delete();

        Session::flash('message','Participante eliminado correctamente');
        return Redirect::to('/ciclistas/'.$carrera['id']);
    }

    public function buscar(Request $request){
        $carrera = Carrera::find($request->carrera_id);
        $participantes = $carrera->participantes()->where('num_dorsal', $request->num_dorsal)->paginate(10);
        return view('carreras.participantes.index',compact('participantes','carrera'));

    }

    public function show($id, Request $request){
        $participante = Participante::where('id',$id)->firstOrFail();
        $ubicacion = UbicacionHistorico::where('participante_id',$participante['id'])->orderBy('id','desc')->paginate(10);
        $carrera = $participante->carrera()->firstOrFail();
        return view('carreras.participantes.informacion',compact('participante','ubicacion','carrera'));

    }

    public function borrarUbicacion(Request $request){
        $rules = array(
            'carrera' => 'required',
            
        );

          $mensajes = array(
            'carrera.required' => 'el id de la carrera es es requerido',
        );
          $validator = Validator::make(Input::all(), $rules,$mensajes);

          if ($validator->fails()) 

            return Response::json(array('success'=>false,'errors' => $validator->errors()->all()));
        else{

        $ubicaciones = UbicacionHistorico::where('carrera_id',$request->carrera)->delete();
        $success=array('success'=>true,'mensaje'=>'Historico borrado!!');
         return response()->json($success);
        }
    }


}
