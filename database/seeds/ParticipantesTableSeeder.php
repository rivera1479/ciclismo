<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ParticipantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('participantes')->insert([
            'nombre' => 'Marcos',
            'apellido' => 'Chevro',
            'dni' => 2832550883,
            'num_dorsal' => 1,
            'telefono' => 2832550883,
            'direccion' => 'tronconal',
            'carrera_id' => 1,

        ]);

        DB::table('participantes')->insert([
            'nombre' => 'Pedro',
            'apellido' => 'Carmelo',
            'dni' => 8685468,
            'num_dorsal' => 2,
            'telefono' => 4143837314,
            'direccion' => 'Lecherias',
            'carrera_id' => 1,

        ]);

        DB::table('participantes')->insert([
            'nombre' => 'Mateo',
            'apellido' => 'Perez',
            'dni' => 4264840899,
            'num_dorsal' => 3,
            'telefono' => 4264840899,
            'direccion' => 'Guanta',
            'carrera_id' => 2,

        ]);

        DB::table('participantes')->insert([
            'nombre' => 'Luis',
            'apellido' => 'Andrade',
            'dni' => 658354712,
            'num_dorsal' => 4,
            'telefono' => 65835471223,
            'direccion' => 'Guanipa',
            'carrera_id' => 2,

        ]);
    }
}
