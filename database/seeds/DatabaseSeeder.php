<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(CarreraTableSeeder::class);
            // La creación de datos de roles debe ejecutarse primero
        //Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);
        $this->call(PuntosControlTableSeeder::class);
        $this->call(PatrocinadoresTableSeeder::class);
        $this->call(ParticipantesTableSeeder::class);
        $this->call(InscritosTableSeeder::class);
        $this->call(ParticipanteUserTableSeeder::class);
        $this->call(TiempoTableSeeder::class);
        $this->call(UbicacionHistoricoTableSeeder::class);

    }
}
