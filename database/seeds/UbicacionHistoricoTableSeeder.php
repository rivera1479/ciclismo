<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UbicacionHistoricoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ubicacion_historico')->insert([
            'latitud' => '-10.24725371',
            'longitud' => '23.2212124',
            'fecha' => '10/10/18',
            'hora' => '10:10:10',
            'carrera_id'=> 1,
            'participante_id'=>1
        ]);
    }
}
