<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TiempoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tiempos')->insert([
            'tiempo' => '00:15:23',
            'puntos_control_id' => 1,
            'participante_id' => 1,
            'km' => 12,
            'nombre_control' => 'Nombre de punto'

        ]);

        DB::table('tiempos')->insert([
            'tiempo' => '00:22:33',
            'puntos_control_id' => 2,
            'participante_id' => 1,
            'km' => 12,
            'nombre_control' => 'Nombre de punto'
        ]);

        DB::table('tiempos')->insert([
            'tiempo' => '00:16:33',
            'puntos_control_id' => 3,
            'participante_id' => 1,
            'km' => 12,
            'nombre_control' => 'Nombre de punto'
        ]);
    }
}
