<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PuntosControlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('puntos_controls')->insert([
            'titulo' => 'Carrera 1 Punto 1',
            'km' => '15',
            'carrera_id' => '1',
            'tipo_punto' => 'Punto de control'
        ]);
        
        DB::table('puntos_controls')->insert([
            'titulo' => 'Carrera 1 Punto 2',
            'km' => '18',
            'carrera_id' => '1',
            'tipo_punto' => 'Punto de control'
        ]);

        DB::table('puntos_controls')->insert([
            'titulo' => 'Carrera 1 Punto 3',
            'km' => '20',
            'carrera_id' => '1',
            'tipo_punto' => 'Punto de control'
        ]);

        DB::table('puntos_controls')->insert([
            'titulo' => 'Carrera 2 Punto 1',
            'km' => '18',
            'carrera_id' => '2',
            'tipo_punto' => 'Punto de control'
        ]);

        DB::table('puntos_controls')->insert([
            'titulo' => 'Carrera 2 Punto 2',
            'km' => '25',
            'carrera_id' => '2',
            'tipo_punto' => 'Punto de control'
        ]);

        DB::table('puntos_controls')->insert([
            'titulo' => 'Carrera 2 Punto 3',
            'km' => '5',
            'carrera_id' => '2',
            'tipo_punto' => 'Punto de control'
        ]);
    }
}
