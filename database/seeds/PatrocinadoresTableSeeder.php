<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PatrocinadoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patrocinadors')->insert([
            'nombre' => 'Pepsi',
            'url' => 'http://pepsi.com',
            'img' => 'pepsi.jpg',
            'principal' => false,
            'carrera_id' => 1,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Adidas',
            'url' => 'http://adidas.com',
            'img' => 'adidas.jpg',
            'principal' => false,
            'carrera_id' => 1,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Carrefour',
            'url' => 'http://carrefour.com',
            'img' => 'carrefour.jpg',
            'principal' => false,
            'carrera_id' => 1,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Energy Fig',
            'url' => 'http://energyfig.com',
            'img' => 'energif.png',
            'principal' => false,
            'carrera_id' => 1,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Movistar',
            'url' => 'http://movistar.com',
            'img' => 'movistar.png',
            'principal' => true,
            'carrera_id' => 1,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Adidas',
            'url' => 'http://adidas.com',
            'img' => 'adidas.jpg',
            'principal' => false,
            'carrera_id' => 2,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Cocacola',
            'url' => 'http://cocacola.com',
            'img' => 'adidas.jpg',
            'principal' => false,
            'carrera_id' => 2,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Restaurante la portada de mediodia',
            'url' => 'http://restaurantelaportada.com',
            'img' => 'restaurante.jpg',
            'principal' => false,
            'carrera_id' => 2,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Segovia',
            'url' => 'http://segovia.com',
            'img' => 'segovia.jpg',
            'principal' => false,
            'carrera_id' => 2,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Le coq sportif',
            'url' => 'http://sportif.com',
            'img' => 'sportif.jpg',
            'principal' => false,
            'carrera_id' => 2,
        ]);

        DB::table('patrocinadors')->insert([
            'nombre' => 'Movistar',
            'url' => 'http://movistar.com',
            'img' => 'movistar.png',
            'principal' => true,
            'carrera_id' => 2,
        ]);

    }
}
