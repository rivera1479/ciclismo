<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class InscritosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('inscripcions')->insert([
            'fecha' => '2018-06-06',
            'carrera_id' => '1',
            'participante_id' => '1',
        ]);

        DB::table('inscripcions')->insert([
            'fecha' => '2018-06-06',
            'carrera_id' => '1',
            'participante_id' => '2',
        ]);

        DB::table('inscripcions')->insert([
            'fecha' => '2018-06-06',
            'carrera_id' => '2',
            'participante_id' => '3',
        ]);

        DB::table('inscripcions')->insert([
            'fecha' => '2018-06-06',
            'carrera_id' => '2',
            'participante_id' => '4',
        ]);
    }
}
