<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_api = Role::where('name', 'api')->first();
        
        $user = new User();
        $user->nombre = 'admin';
        $user->email = 'admin@test.com';
        $user->password = '123456';
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->nombre = 'Dorsal 1';
        $user->num_dorsal = 1;
        $user->username = 11;
        $user->password = '123456';
        $user->id_carrera = 1;
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->nombre = 'Dorsal 1';
        $user->num_dorsal = 1;
        $user->username = '1.1';
        $user->password = '123456';
        $user->id_carrera = 1;
        $user->save();
        $user->roles()->attach($role_api);

        $user = new User();
        $user->nombre = 'Dorsal 2';
        $user->num_dorsal = 2;
        $user->username = 12;
        $user->password = '123456';
        $user->id_carrera = 1;
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->nombre = 'Dorsal 2';
        $user->num_dorsal = 2;
        $user->username = '1.2';
        $user->password = '123456';
        $user->id_carrera = 1;
        $user->save();
        $user->roles()->attach($role_api);

        $user = new User();
        $user->nombre = 'Dorsal 3';
        $user->num_dorsal = 3;
        $user->username = 23;
        $user->password = '123456';
        $user->id_carrera = 2;
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->nombre = 'Dorsal 3';
        $user->num_dorsal = 3;
        $user->username = '2.3';
        $user->password = '123456';
        $user->id_carrera = 2;
        $user->save();
        $user->roles()->attach($role_api);

        $user = new User();
        $user->nombre = 'Dorsal 4';
        $user->num_dorsal = 4;
        $user->username = 24;
        $user->password = '123456';
        $user->id_carrera = 2;
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->nombre = 'Dorsal 4';
        $user->num_dorsal = 4;
        $user->username = '2.4';
        $user->password = '123456';
        $user->id_carrera = 2;
        $user->save();
        $user->roles()->attach($role_api);
    }
}
