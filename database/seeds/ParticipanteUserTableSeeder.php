<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ParticipanteUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('participante_user')->insert([
            'participante_id' => '1',
            'user_id' => '2'
        ]);

        DB::table('participante_user')->insert([
            'participante_id' => '1',
            'user_id' => '3'
        ]);

        DB::table('participante_user')->insert([
            'participante_id' => '2',
            'user_id' => '4'
        ]);

        DB::table('participante_user')->insert([
            'participante_id' => '2',
            'user_id' => '5'
        ]);

        DB::table('participante_user')->insert([
            'participante_id' => '3',
            'user_id' => '6'
        ]);

        DB::table('participante_user')->insert([
            'participante_id' => '3',
            'user_id' => '7'
        ]);

        DB::table('participante_user')->insert([
            'participante_id' => '4',
            'user_id' => '8'
        ]);

        DB::table('participante_user')->insert([
            'participante_id' => '4',
            'user_id' => '9'
        ]);
    }
}
