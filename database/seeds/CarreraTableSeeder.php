<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarreraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carreras')->insert([
            'nombre' => 'Carrera de ciclismo 1',
            'descripcion' => 'Descripcion ciclismo 1',
            'logo' => 'logo-perico-2018.jpg',
            'img_cabecera' => 'ciclismo1.jpeg',
            'url' => 'carrera_de_ciclismo_1',
            'km_totales' => 15,
            'fecha' => '2018-06-06',
            'url_encuesta' => 'https://docs.google.com/forms/d/e/1FAIpQLSdNPTfo0HCRbvxUPnDvhQxLNaOQz1c3VD50rK3ZvXD9tvCA-Q/viewform',
        ]);
        
        DB::table('carreras')->insert([
            'nombre' => 'Carrera de ciclismo 2',
            'descripcion' => 'Descripcion ciclismo 2',
            'logo' => 'logo-perico-2018.jpg',
            'img_cabecera' => 'ciclismo2.jpeg',
            'url' => 'carrera_de_ciclismo_2',
            'km_totales' => 65,
            'fecha' => '2018-06-06',
        ]);
    }
}
