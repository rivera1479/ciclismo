<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>csv</title>
</head>
<body>
	<div id="listo"></div>
<form class="form-inline">
<div class="form-group">
    <input type="hidden" id="url_base" value="{{ url('') }}">
 
      <label for="files">Upload a CSV formatted file:</label>
      <input type="file" id="files" class="form-control" accept=".csv" required />
    </div>
 
<div class="form-group">
     <button type="submit" id="submit-file" class="btn btn-primary">Upload File</button>
     </div>
</form>



	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
	<script src="js/papaparse/papaparse.min.js"></script>
	

	<script>

        $('#submit-file').on("click",function(e){
    e.preventDefault();
    $('#files').parse({
        config: {
            delimiter: "auto",
            complete: completeFn,
        },
        before: function(file, inputElem)
        {
            console.log("Parsing file...", file);
        },
        error: function(err, file)
        {
            console.log("ERROR:", err, file);
        },
        complete: function()
        {
            console.log("Done with all files");
        }
    });

    function enableButton()
{
    $('#submit-file').prop('disabled', false);
}

var inputType = "string";
var stepped = 0, rowCount = 0, errorCount = 0, firstError;
var start, end;
var firstRun = true;
var maxUnparseLength = 10000;
function printStats(msg)
{
    if (msg)
        console.log(msg);
    console.log("       Time:", (end-start || "(Unknown; your browser does not support the Performance API)"), "ms");
    console.log("  Row count:", rowCount);
    if (stepped)
        console.log("    Stepped:", stepped);
    console.log("     Errors:", errorCount);
    if (errorCount)
        console.log("First error:", firstError);
}
    function completeFn(results)
{
    end = now();

    if (results && results.errors)
    {
        if (results.errors)
        {
            errorCount = results.errors.length;
            firstError = results.errors[0];
        }
        if (results.data && results.data.length > 0)
            rowCount = results.data.length;
    }

    printStats("Parse complete");

    console.log("    Resultados:", results.data);

var data = results.data; 
$.ajax({

    url:$('#url_base').val()+'/guardarcvs',
    type:'POST',
    data:{data, _token: '{{csrf_token()}}' },
    success: function(data){
        if((data.success==true)){
            var nombre = data.nombre;
            var carrera_id = data.carrera_id;
            $("#listo").html('<p>Listo</p>');
            $("#listo").html(nombre);
            console.log('Listo');
            console.log(carrera_id);
            
        }
        else{
            console.log('no listo');
            var mensaje= '';
            for(var i=0;i<data.errors.length;i++){
            mensaje+=data.errors[i]+'<br>';
        }
            $("#listo").html('<p>No esta listo</p>');
        }

    }

});

    // icky hack
    setTimeout(enableButton, 100);
}

function now()
{
    return typeof window.performance !== 'undefined'
            ? window.performance.now()
            : 0;
}
});

</script>

</body>
</html>