@extends('layouts.template')
@section('content')
          <div class="row">
            <div class="col col-login mx-auto" style="margin-top: 2rem;">
              
              <form class="card" action="{{ route('login') }}" method="post">
                @csrf
                <div class="card-body p-6">
                  <div class="card-title">Login</div>
                  <div class="form-group">
                    <label class="form-label">Usuario</label>
                    <input id="login" type="text" class="form-control" placeholder="Ingrese usuario o correo" name="login" value="{{ old('login') }}" required autofocus>
                                @if ($errors->has('login'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                @endif
                  </div>
                  
                  <div class="form-group">
                    <label class="form-label">
                      Contraseña
                    </label>
                
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                  </div>

                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
@endsection