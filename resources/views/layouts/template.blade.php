<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Sistema de ciclismo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
	<script src="{{ asset('/assets/js/vendors/jquery-3.2.1.min.js') }}"></script>

	<script src="{{ asset('/assets/js/require.min.js') }}"></script>
	<script>
		requirejs.config({
			baseUrl: '{{ url('') }}'
		});
	</script>
	<link href="{{ asset('/assets/css/dashboard.css') }}" type="text/css" rel="stylesheet" />
	<script src="{{ asset('/assets/js/dashboard.js') }}"></script>

	<!-- c3.js Charts Plugin -->
	<link href=" {{ asset('/assets/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
	<script src=" {{ asset('/assets/plugins/charts-c3/plugin.js') }}"></script>
	<!-- Google Maps Plugin -->
	<link href=" {{ asset('/assets/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
	<script src=" {{ asset('/assets/plugins/maps-google/plugin.js') }}"></script>
	<!-- Input Mask Plugin -->
	<script src=" {{ asset('/assets/plugins/input-mask/plugin.js') }}"></script>
	<script src=" {{ asset('/assets/plugins/jssor.slider.min.js') }}"></script>
	<link href="{{ asset('/css/administrador.css') }}" type="text/css" rel="stylesheet" />
	<link href="{{ asset('/js/slim/slim.min.css') }}" type="text/css" rel="stylesheet" />



</head>
<body class="">
	<input type="hidden" id="url_base" value="{{ url('') }}">
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	{{-- Inicio de pagina --}}
	<div class="page">
		<div class="page-main">
			{{-- Encabezado del template --}}
			<div class="header py-4">
				<div class="container">
					<div class="row align-items-center text-center">
						{{-- Si esta en el area de "seguimiento al corredor" carreras tomara el logo de la carrera --}}
						@if(isset($race))
						<div class="col-md-3">
							<a class="header-brand align-self-center" href="{{asset('/')}}">
								<img src="{{asset('/img/'.$race['logo'] )}}" class="img-responsive"  alt="Ciclismo">
							</a>
						</div>

						{{-- Titulo de la carrera y sus km --}}
						<div class="col-md-6">
							<h2 class="text-center">{{$race['nombre']}}</h2>
							<div class="text-center">
								<div class="text-default">{{$race['km_totales']}} KM totales</div>
								<small class="d-block text-muted">{{$race['fecha']}}</small>
							</div>
						</div>
						{{-- Fin del titulo de la carrera y sus km --}}

						{{-- Patrocinador principal --}}			
						<div class="col-md-3">
							@foreach($patrocinadores as $patrocinador)
							@if($patrocinador['principal']==1)
							<a href="{{ $patrocinador["url"]}}">
								<img src="{{asset('/img/'.$patrocinador['img'] )}}" class="img-responsive">
							</a>
							@endif
							@endforeach
							<a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0 d-block" data-toggle="collapse" data-target="#headerMenuCollapse">
								<span class="header-toggler-icon"></span>
							</a>
						</div>


						{{-- De lo contrario se establecera un header por defecto --}}
						@else

						<div class="col-md-3">
							<a class="header-brand align-self-center" href="{{asset('/')}}">
								<img src="{{ asset('/images/logo.jpg') }}" class="img-responsive" alt="Logo" style="width: 100px">
							</a>
						</div>
							
						<div class="col-md-6 text-center">
							<h2>SEGUIMIENTO DE CARRERAS</h2>
						</div>

						<div class="col-md-3 text-right">
							
						@auth
						{{-- Menu de usuario en dropdown --}}
						
							<div class="dropdown">
								<a href="" class="nav-link pr-0 leading-none" data-toggle="dropdown">
									<span class="avatar" style="background-image: url('img/user.png')"></span>
									<span class="ml-2 d-none d-lg-block">
										<span class="text-default">{{ Auth::user()->nombre }}</span>
										<small class="text-muted d-block mt-1">
											@php 
											$roles = Auth::user()->roles; 
											@endphp
											@foreach($roles as $rol)
											{{ $rol->description }}
											@endforeach
										</small>
									</span>
								</a>

								<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
									<a class="dropdown-item" href="{{ route('logout') }}"
										onclick="event.preventDefault();
										document.getElementById('logout-form').submit();">
										<i class="dropdown-icon fe fe-log-out"></i>
										{{ __('Salir') }}
									</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@csrf
									</form>
								</div>
								
							</div>
						
						{{-- Fin de menu de usuario en dropdown --}}
						@endauth
							<a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0 d-block" data-toggle="collapse" data-target="#headerMenuCollapse">
								<span class="header-toggler-icon"></span>
							</a>
						@endif
					
							
						</div>


					</div>

				</div>
				
			</div>
			{{-- Fin de header --}}


			{{-- Menu principal --}}
			<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
				<div class="container">
					<div class="row align-items-center">
						<!-- Items de menu superior -->
						<div class="col-lg order-lg-first">
							<ul class="nav nav-tabs border-0 flex-column flex-lg-row">
								<li class="nav-item">
									<a href="{{url('/')}}" class="nav-link"><i class="fe fe-home"></i> Inicio</a>
								</li>
								@isset($race)
								{{-- Sección de "Seguir al corredor" --}}
								@if($race['url_encuesta'] == !null)
								<li class="nav-item">
									<a href="{{ $race['url_encuesta'] }}" class="nav-link"><i class="fa fa-question-circle-o"></i> Ayudanos a mejorar</a>
								</li>
								@endif
								{{-- Fin de sección de "seguir al corredor"--}}
								@else
								{{-- En caso contrario de que no este en la sección de "Seguir al corredor" --}}
								@guest
								@else
								{{-- Menu del administrador --}}

								@if(Auth::user()->roles()->first()->id == 1)
								<li class="nav-item">
									<a href="{{url('carreras')}}" class="nav-link"><i class="fe fe-box"></i> Carreras</a>
								</li>

								<li class="nav-item dropdown">
									<a href="{{url('usuarios')}}" class="nav-link"><i class="fe fe-calendar"></i> Usuarios</a>
								</li>
								{{--Fin del menu del administrador --}}
								@else
								{{-- Enlace los administradores de tiempo  --}}
								<li class="nav-item">
									<a href="{{ url('participantes') }}" class="nav-link"><i class="fe fe-file-text"></i>Participantes</a>
								</li>
								{{-- Fin de enlace los administradores de tiempo  --}}
								@endif

								@endguest
								{{-- Fin de en caso contrario de que no este en la sección de "Seguir al corredor" --}}
								@endisset
							</ul>
						</div>
						<!-- Fin de items de menu superior -->
					</div>
				</div>
				
			</div>
			{{-- Fin del menu principal --}}
			{{-- Contenido de pagina --}}
			@yield('content')
		</div>
			





<footer>
	<div class="design">
		<p>Diseño y Desarrollo por <a href="https://www.audituxinformatica.com/" target="blank"><img src="{{ asset('/images/auditux-logo.png') }}" alt="Auditux Informatica" style=" vertical-align: middle;"></a></p>
	</div>
</footer>

<script src="{{ asset('js/Template.js') }}"></script>
<script src="{{ asset('js/password.js') }}"></script>
<script src="{{ asset('js/passwordEdit.js') }}"></script>
<script src="{{ asset('js/slim/slim.kickstart.min.js') }}"></script>
</body>
</html>