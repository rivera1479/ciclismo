<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">

    @isset($participante)
    <title>{{$participante['num_dorsal']}} - {{$participante['nombre']}} {{$participante['apellido']}} | TuPosition</title>
    <meta name="description" content="Seguimiento de corredores en la carrera {{$race['nombre']}} - {{date('l jS \of F Y h:i:s A')}}, {{$participante['num_dorsal']}}, {{$participante['nombre']}} {{$participante['apellido']}}">
    <meta name="keywords" content="tuposition,{{$race['nombre']}}, {{$participante['nombre']}} {{$participante['apellido']}}" />
    @else
	<title>{{$race['nombre']}} | TuPosition</title>
    <meta name="description" content="Seguimiento de corredores en la carrera {{$race['nombre']}} - {{date('l jS \of F Y h:i:s A')}}">
    <meta name="keywords" content="tuposition,{{$race['nombre']}}" />    
    @endisset
    <meta name="DC.title" content="TuPosition">
    <meta property="og:title" content="TuPosition">
    <meta property="og:description" content="Seguimiento de corredores en las distintas carreras patrocinadas por TuPosition">
    <meta property="og:locale" content="es_ES">
    <meta property="og:site_name" content="TuPosition">
    <meta property="og:url" content="https://tuposition.es/">
    <meta property="og:image" content="https://tuposition.es/img/{{ $race['img_cabecera'] }}">
    <meta property="og:type" content="website">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="distribution" content="all">
    <meta name="robots" content="all">
    <meta name="revisit" content="7 days">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="TuPosition">
    <meta name="twitter:image" content="https://tuposition.es/img/{{ $race['img_cabecera']}}">
    <meta name="twitter:description" content="Seguimiento de corredores en las distintas carreras patrocinadas por TuPosition">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	{{-- {!!Html::style('assets/css/bootstrap.min.css')!!} --}}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
	{{-- <link rel="stylesheet" href="{{ asset('/assets/fonts/font-awesome.min-4.7.0.css') }}"> --}}
	{{-- <link rel="stylesheet" href="{{ asset('/assets/fonts/font-sans.css') }}"> --}}

	<script src="{{ asset('/assets/js/vendors/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('/assets/js/require.min.js') }}"></script>
	
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet"> 
	<script>
		requirejs.config({
			baseUrl: '{{ url('') }}'
		});
	</script>
	<link href="{{ asset('/assets/css/dashboard.css') }}" type="text/css" rel="stylesheet" />
	<script src="{{ asset('/assets/js/dashboard.js') }}"></script>

	<!-- c3.js Charts Plugin -->
	<link href=" {{ asset('/assets/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
	<script src=" {{ asset('/assets/plugins/charts-c3/plugin.js') }}"></script>
	<!-- Google Maps Plugin -->
	<link href=" {{ asset('/assets/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
	<script src=" {{ asset('/assets/plugins/maps-google/plugin.js') }}"></script>
	<!-- Input Mask Plugin -->
	<script src=" {{ asset('/assets/plugins/input-mask/plugin.js') }}"></script>
	<script src=" {{ asset('/assets/plugins/jssor.slider.min.js') }}"></script>
	<link href="{{ asset('/css/app.css') }}" type="text/css" rel="stylesheet" />
	


</head>
<body class="">
	<input type="hidden" id="url_base" value="{{ url('') }}">
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	
	{{-- Inicio de pagina --}}
	<div class="page">
		<div class="page-main">
			<div class="container" style="background-color: #F5F7FB;">
				<div class="row align-items-center"  style="background-color: white">
					{{-- Si esta en el area de "seguimiento al corredor" carreras tomara el logo de la carrera --}}
					@if(isset($race))
					<div class="col-md-3 text-center" style="background-color: white">
						<a class="header-brand align-self-center" href="{{asset('/')}}">
							 <img src="{{asset('/img/'.$race['logo'] )}}" class="img-responsive"  alt="Ciclismo" style="max-height: 150px;">
						</a>
					</div>
					{{-- Titulo de la carrera y sus km --}}
					<div class="col-md-6 text-center" style="background-color: white">
						<h2 class="text-center titulo">{{$race['nombre']}}</h2>
						<div class="text-center">
							<div class="text-default">{{$race['km_totales']}} KM totales</div>
							<small class="d-block text-muted">{{$race['fecha']}}</small>
						</div>
					</div>
					{{-- Fin del titulo de la carrera y sus km --}}

					{{-- Patrocinador principal --}}
					<div class="col-md-3 text-center">
						@foreach($patrocinadores as $patrocinador)
						@if($patrocinador['principal']==1)
						<a href="{{ $patrocinador['url']}}">
							<img src="{{asset('/img/'.$patrocinador['img'] )}}" class="img-responsive" style="max-height: 147px;"> 
						</a>
						@endif
						@endforeach
					</div>
					{{-- De lo contrario se establecera un header por defecto --}}
					@else

					<div class="col-md-3 text-center" >
						<a class="header-brand align-self-center" href="{{asset('/')}}">
							{{-- <img src="{{ asset('/img/logo.jpg') }}" class="img-responsive" alt="Logo"> --}}
							<div style="background:url({{ asset('/img/logo.jpg') }}) center no-repeat;background-size:cover;height: 150px;width: 150px; padding: 0;" class="cabecera">
							</div>
						</a>
					</div>

					<div class="col-md-6 text-center">
						<h2 class="titulo">SEGUIMIENTO DE CARRERA</h2>
					</div>

					<div class="col-md-3 text-center">
						@auth
						{{-- Menu de usuario en dropdown --}}
						<div class="dropdown">
							<a href="" class="nav-link pr-0 leading-none" data-toggle="dropdown">
								<span class="avatar" style="background-image: url('img/user.png')"></span>
								<span class="ml-2 d-none d-lg-block">
									<span class="text-default">{{ Auth::user()->nombre }}</span>
									<small class="text-muted d-block mt-1">
										@php
										$roles = Auth::user()->roles; 
										@endphp
										@foreach($roles as $rol)
										{{ $rol->description }}
										@endforeach
									</small>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
								<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<i class="dropdown-icon fe fe-log-out"></i>
									{{ __('Salir') }}
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
						</div>
						{{-- Fin de menu de usuario en dropdown --}}
						@endauth
						@endif
					</div>







					{{-- Menu principal --}}
                <nav class="navbar navbar-expand-sm navbar-light bg-light" style="padding: 0">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#opciones">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- enlaces -->
                    <div class="menu collapse navbar-collapse" id="opciones">
                        <ul class="nav justify-content-center row" style="background-color: {{$race['descripcion']}};height: 3rem" >
                            <li class="nav-item">
                                <a href="{{url('/')}}" class="nav-link menu-nav" style="color: white"> <strong>INICIO</strong></a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/carreras/'.$race['url'])}}" class="nav-link menu-nav" style="color: white"> <strong>CARRERA</strong></a>
                            </li>
                            @guest
                            @else
                            @if(Auth::user()->roles()->first()->id == 1)
                            <li class="nav-item">
                                <a href="{{url('/carreras')}}" class="nav-link menu-nav" style="color: white"> <strong>ADMINISTRACIÓN</strong></a>
                            </li>
                            @endif
                            @endguest
                        </ul>
                    </div>
                </nav>
					{{-- Fin del menu principal --}}
					{{-- Contenido de pagina --}}
					@yield('content')
				</div>

			</div>
		</div>

	</div>

	<footer>
		<div class="design">
			<p>Diseño y Desarrollo por <a href="https://www.audituxinformatica.com/" target="blank"><img src="{{ asset('/images/auditux-logo.png') }}" alt="Auditux Informatica" style=" vertical-align: middle;"></a></p>
		</div>
	</footer>

	<script src="{{ asset('js/Template.js') }}"></script>

</body>
</html>