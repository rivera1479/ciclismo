@extends('layouts.template')
@section('content')



 <div class="my-3 my-md-12">
	    <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Editar carrera
              </h1>
            </div>
            <div class="col-lg-12">

              <form class="card" method="POST" action="{{route('carreras.update')}}" enctype="multipart/form-data">
              	 @csrf
                 <input type="hidden" value="" name="url">
                 <input type="hidden" value="{{ $carrera->id }}" name="carreraID">
                 <input type="hidden" value="{{ $carrera->fecha }}" name="fecha">
                <div class="card-body">
                  <h3 class="card-title">Editar carrera</h3>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Nombre de carrera</label>
                        <input type="text" class="form-control" placeholder="titulo" name="nombre" value="{{ $carrera->nombre }}">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Encuesta</label>
                        <input type="text" class="form-control" placeholder="Ingrese la url de la encuesta" value="{{$carrera->url_encuesta}}" name="url_encuesta">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Fecha</label>
                        <input type="date" class="form-control" name="fecha" value="{{$carrera->fecha}}">
                      </div>
                    </div>


                    <div class="col-md-4">
                      <div class="form-group mb-0">
                        <label class="form-label">Color</label>
                        <input type="color" name="descripcion" value="{{$carrera->descripcion}}" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="form-label">KM totaltes</label>
                        <input type="text" class="form-control" placeholder="" value="{{ $carrera->km_totales }}" name="km_totales" disabled>
                      </div>
                    </div>

                    <?php
                      $num = [0=> ['visibility' =>1,'name' => 'Activo'],1=>['visibility' =>0,'name' => 'Inactivo'],2=>['visibility' =>2,'name' => 'Deshabilitado'] ];
                    ?>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="form-label">Estado</label>
                        <select name="visibility" id="visibility" class="form-control">
                          @foreach($num as $variable)
                            @if($variable['visibility'] == $carrera->visibility)
                            <option value="{{$variable['visibility']}}" selected="">{{$variable['name']}}</option>
                            @else
                            <option value="{{$variable['visibility']}}">{{$variable['name']}}</option>
                            @endif
                          @endforeach
                          
                        </select>
                      </div>
                    </div>
    
                    
                    <div class="form-group col-md-6">
                        <div class="form-label">Subir logo de carrera</div>
                        <div class="slim" data-force-size="1024,1024">
                          @empty($carrera->logo)
                          @else
                          <img src="{{asset('/img/'.$carrera->logo)}}" alt=""/>
                          @endempty
                          <input type="file" name="logo" value="{{ $carrera->logo }}">
                          <label class="custom-file-label">Elegir imagen</label>
                          
                        </div>
                        
                    </div>
                    <div class="form-group col-md-6">
                        <div class="form-label">Subir imagen de cabecera de carrera</div>
                        <div class="slim" data-force-size="1920,600">
                          @empty($carrera->img_cabecera)
                          @else
                          <img src="{{asset('/img/'.$carrera->img_cabecera)}}" alt=""/>
                          @endempty
                          <input type="file" name="img_cabecera" value="{{ $carrera->img_cabecera}}">
                          <label class="custom-file-label">Elegir imagen</label>
                        </div>
                        
                    </div>
                  </div>
                </div>
			</div>



                <div class="card-footer text-right">
                  <a href="{{ asset('/carreras/') }}" class="btn btn-danger">Atras</a>
                  <button type="submit" class="btn btn-primary">Editar carrera</button>
                </div>
              </form>
        </div>
    </div>

@endsection