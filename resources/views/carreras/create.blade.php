@extends('layouts.template')
@section('content')
 <div class="my-3 my-md-5">
	    <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Crear carrera
              </h1>
            </div>
            <div class="col-lg-12">

              <form class="card" method="POST" action="{{route('carreras.store')}}" enctype="multipart/form-data">
              	 @csrf
                 
                 
                <div class="card-body">
                  <h3 class="card-title">Crear carrera</h3>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Titulo carrera</label>
                        <input type="text" class="form-control" placeholder="Ingrese titulo de la carrera" value="" name="nombre">
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Encuesta</label>
                        <input type="text" class="form-control" placeholder="Ingrese la url de la encuesta" value="" name="url_encuesta">
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Fecha</label>
                        <input type="date" class="form-control" name="fecha">
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group mb-0">
                        <label class="form-label">Color</label>
                        <input type="color" name="descripcion" class="form-control">
                      </div>
                    </div>

                     <div class="col-md-3">
                      <div class="form-group">
                        <label class="form-label">KM totaltes</label>
                        <input type="text" class="form-control" disabled  name="km_totales" value="0">
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="form-label">Estado</label>
                        <select name="visibility" id="visibility" class="form-control">
                          <option value="0">Inactivo</option>
                          <option value="1">Activo</option>
                          <option value="2">Deshabilitado</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group col-md-6">
                      <div class="form-label">Subir logo de carrera</div>
                      <div class="slim" data-force-size="1024,1024">
                        <input type="file" name="logo" >
                        <label class="custom-file-label">Elegir imagen</label>
                      </div>
                    </div>
                    <div class="form-group col-md-6">
                      <div class="form-label">Subir imagen de cabecera de carrera</div>
                      <div class="slim" data-force-size="1920,600">
                        <input type="file" name="img_cabecera">
                        <label class="custom-file-label">Elegir imagen</label>
                      </div>
                    </div> 
                  </div>
                </div>
			</div>



                <div class="card-footer text-right">
                  <a href="{{ asset('/carreras/') }}" class="btn btn-danger">Atras</a>
                  <button type="submit" class="btn btn-primary">Crear carrera</button>
                </div>
              </form>
        </div>
    </div>
@endsection