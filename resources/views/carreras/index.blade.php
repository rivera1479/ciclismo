@extends('layouts.template')
@section('content')
@include('alerts.messages')
 <div class="my-3 my-md-5">
        <div class="container">
            <div class="page-header d-flex">
              <h1 class="page-title mr-auto">
               Carreras
              </h1>
              <div class="p2">
                <a href="{{route('carreras.create')}}" class="btn btn-primary mr-auto"> Crear carrera</a>
              </div>
            </div>

            

              <div class="col-12">
                <div class="card">
                 
                  <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap">
                      <thead>
                        <tr>
                          <th class="w-1">Nombre</th>
                          <th>Logo</th>
                          <th>Imagen Cabecera</th>
                          <th>KM totales</th>
                          <th>Participantes</th>
                          <th>Fecha</th>
                          <th>Estatus</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($carreras as $carrera)
                        <tr>
                          <td><span class="text-muted">{{ $carrera['nombre'] }}</span></td>
                          <td><img src="{{ asset('/img/'.$carrera['logo']) }}" alt="" style="width: 200px;"></td>
                          <td><img src="{{ asset('/img/'.$carrera['img_cabecera']) }}" alt="" style="width: 200px;"></td>
                          <td>{{ $carrera['km_totales'] }}</td>
                          <td>{{count($carrera->participantes()->get())}}</td>
                          <td>{{ $carrera['fecha'] }}</td>
                          <td>
                            <input type="hidden" value="{{$carrera['status']}}">
                            <a href="javascript:void(0)" class="status test"></a>
                            <input type="hidden" value="{{$carrera->id}}">
                          </td>
                            
                          <td class="text-right">
                            <a class="icon" href="{{ route('carreras.edit', $carrera)}}" title="Editar carrera"><i class="fe fe-edit"></i></a>
                            <a class="icon" href="punto/{{ $carrera->id }}" title="Puntos de control de carrera"><i class="fa fa-bicycle"></i></a>
                            <a class="icon" href="patrocinadores/{{$carrera->id}}" title="Patrocinadores"><i class="fa fa-money"></i></a>
                            <a class="icon" href="{{ route('formimport',$carrera->id) }}" title="Cargar csv participantes"><i class="fa fa-users"></i></a>
                            <a class="icon" href="ciclistas/{{$carrera->id}}" title="Participantes"><i class="fe fe-user-check"></i></a>
                            <form action="{{ route('carreras.destroy',$carrera->id)}}" method="POST">
                              @csrf
                              {{ method_field('DELETE') }}
                              
                              <button class="fa fa-trash icon" title="Eliminar carrera"></button>
                            </form>
                          </td>

                        </tr>
                        @endforeach

                      </tbody>
                      
                    </table>
 <center>  {{ $carreras->links() }}</center>
                  </div>

                </div>

              </div>
    </div>

</div>

    <script>
      $(document).ready(function(){

        $('.status').each(function(){
          var btn = $(this).prev().val();
          if (btn == 0) {
            $(this).addClass('btn btn-danger').html('No iniciada');
          }
          else{
            $(this).addClass('btn btn-primary').html('Iniciada');
          }
        });


        $('.test').on("click",function(){
          
          var status = $(this).prev().val();

          var carrera_id = $(this).next().val();
          var btn = $(this);
          $.ajax({
            url:$('#url_base').val()+'/status/'+status,
            type:'POST',
            data:{
              status,
              _token: '{{csrf_token()}}',
              carrera_id
            },
              success: function(data){
                if((data.success==true)){
                  if (data.new_status == 1)
                  {
                    btn.prev().val('1');
                    btn.removeClass().addClass('btn btn-primary').html('Iniciada');
                  }
                  else
                  {
                    btn.prev().val('0');
                    btn.removeClass().addClass('btn btn-danger').html('No iniciada');
                  }
                }
                else{

                }
              }
            });

        });
      });


      

    </script>
@endsection