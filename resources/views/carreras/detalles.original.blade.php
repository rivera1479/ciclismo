@extends('layouts.template')
@section('content')
@php
$participantes= $race->participantes()->get();
$patrocinadores= $race->patrocinadores()->get();

@endphp

<script src="https://www.google.com/jsapi"></script>
<div class="my-3 my-md-5">

  {{-- Container de toda la pagina --}}
  <div class="container">
    {{-- Header de numero de dorsal --}}
    @isset($participante)
    <div class="page-header">
              <h1 class="page-title">
                  Num Dorsal: {{$participante['num_dorsal']}}, Nombre: {{$participante['nombre']}} {{$participante['apellido']}}
              </h1>
    </div>
    @endisset
    
    <div class="row row-cards">

      <div class="col-12">
        <div class="card">
          <img src="{{asset('/img/'.$race['img_cabecera'])}}" alt="{{$race['nombre']}}" class="img-responsive">        
        </div>
      </div>

      @if(!isset($participante))
    @include('carreras.mapas.withoutparticipants')
  @else
    @isset($ubicacion)
      <div class="col-12 col-lg-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Ruta de carrera</h3>
          </div>

          <div class="card-body">
            <div id="map" class="map"></div>

          </div>
        </div>
      </div>

            <div class="col-12 col-lg-12">
              <div class="card">
                <div id="elevation_chart" class="card-map card-map-placeholder map"></div>
              <div  id="map-elevation" class="hidden"></div>  
              </div>
            </div>
</div>
    @endisset

    
    
    
      
    
    <div class="row row-cards row-deck">
          

<div class="col-12 col-lg-12">
  <div class="card">
            <div class="card-header">
              <h3 class="card-title">Gráfica</h3>
            </div>
            <div class="card-body">
              
              <div id="chart-temperature" class="c3" style="height: 16rem"></div>

            </div>
            </div>
</div>

           {{-- Tabla de tiempos --}}
<div class="col-12 col-lg-12">
  <div class="card">
              <div class="table-responsive">
                <table class="table card-table table-striped table-vcenter">
                  <thead>
                  <tr>
                    <th>N° Control</th>
                    <th class="text-center">Tiempo</th>
                    <th class="text-center">Tiempo total</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php $control = 0; @endphp
                    @foreach($tiempos as $tiempo)
                      <tr>
                        <td>KM: {{$puntosControl[$control]['km']}}</td>
                        @php
                        $control = $control + 1;
                      @endphp
                        <td class="text-center"><i class="fa fa-clock-o text-muted"></i> {{$tiempo['tiempo_actual']}}</td>
                        <td class="text-center"><i class="fa fa-clock-o text-muted"></i> {{$tiempo['tiempo_total']}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
</div>
            </div>

</div>   
<script>
        var map;
        var pathCoordinates = Array();
        function initMap() {
                var countryLength
                var mapLayer = document.getElementById("map-layer");
                var centerCoordinates = new google.maps.LatLng(37.6, -95.665);
                var defaultOptions = {
                        center : centerCoordinates,
                        zoom : 4
                }
                map = new google.maps.Map(mapLayer, defaultOptions);
                geocoder = new google.maps.Geocoder();
                <?php
            if (! empty($countryResult)) {
            ?>
                countryLength = <?php echo count($countryResult); ?>
                <?php
                foreach ($countryResult as $k => $v) 
                {
            ?>
                geocoder.geocode({
                        'address' : '<?php echo $countryResult[$k]["country"]; ?>'
                }, function(LocationResult, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                                var latitude = LocationResult[0].geometry.location.lat();
                                var longitude = LocationResult[0].geometry.location.lng();
                                pathCoordinates.push({
                                        lat : latitude,
                                        lng : longitude
                                });

                                new google.maps.Marker({
                                        position : new google.maps.LatLng(latitude, longitude),
                                        map : map,
                                        title : '<?php echo $countryResult[$k]["country"]; ?>'
                                });

                                if (countryLength == pathCoordinates.length) {
                                        drawPath();
                                }

                        }
                });
                <?php
                }
            }
            ?>
        }
        function drawPath() {
                new google.maps.Polyline({
                        path : pathCoordinates,
                        geodesic : true,
                        strokeColor : '#FF0000',
                        strokeOpacity : 1,
                        strokeWeight : 4,
                        map : map
                });
        }

</script>

{{-- Crear mapa en el resultado de la busqueda --}}
<script>

  google.load('visualization', '1', {packages: ['columnchart']});
      function initMap() { 
        var path = [
        @foreach($todosPuntos as $key => $value)
          {lat: {{$value['lat']}},lng:{{$value['lon']}}},
        @endforeach
        ];
      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});


        //Ubicacion para crear un evento en una marca
        var myLatlng = {lat: 40.94082477819901, lng: -3.995340561054263};
        //Markar y etiquetar
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var labelIndex = 0;
        var map = new google.maps.Map(document.getElementById('map'),{
          zoom: 12,
          center: myLatlng
        });
        directionsDisplay.setMap(map);

var mapElevation = new google.maps.Map(document.getElementById('map-elevation'), {
          zoom: 8,
          center:path[1],
          mapTypeId: 'terrain'
        });

        // Create an ElevationService.
        var elevator = new google.maps.ElevationService;

        // Draw the path, using the Visualization API and the Elevation service.
        displayPathElevation(path, elevator, mapElevation);



        function crearRuta() {
          var icons = { 
    start: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/inicio.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
), 
    end: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/finish.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
),
    puntos: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/punto.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
),
    jugador: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/participante.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
),
    puerto: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/marina-2.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
),
    puntoControl: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/clock.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
),
};
          var ori = "{{$origen}}";
          var des = "{{$destino}}";
          var origenArray = ori.split(",");
          var destinoArray = des.split(",");
          var waypoints = [];
          var waypointsCompleto = [];
          var participantes = [];
            var origen = new google.maps.LatLng(parseFloat(origenArray[0]),parseFloat(origenArray[1]));
            var destino =  new google.maps.LatLng(parseFloat(destinoArray[0]),parseFloat(destinoArray[1]));
            @foreach($arrayLatLon as $key => $value)
              var way = new google.maps.LatLng(parseFloat( {{$value['lat']}}),parseFloat( {{$value['lon']}}));
              var tipo_punto = "{{$value['tipo_punto']}}";
              waypoints.push({
                location:way,
                stopover: true});
              waypointsCompleto.push({
                tipo_punto : tipo_punto
              });
            @endforeach

            @foreach($ubicacion as $datos)
              var participante =  new google.maps.LatLng(parseFloat({{$datos['latitud']}}),parseFloat({{$datos['longitud']}}));;
              participantes.push(participante);
            @endforeach

          directionsService.route({
            origin:  origen ,
            destination: destino,
            waypoints:  waypoints,
            travelMode: 'BICYCLING'
          }, function(response, status){
            if(response, status){              
              directionsDisplay.setDirections(response);
              var leg = response.routes[ 0 ].legs[ 0 ]; 
              makeMarker(leg.start_location, icons.start, "Inicio de punto"); 
              makeMarker(destino, icons.end, 'Final de punto');
              for (var i = 0; i < waypoints.length; i++) {
                if (waypointsCompleto[i]['tipo_punto'] == 'Puerto') {
                  makeMarker(waypoints[i]['location'], icons.puerto, 'Puerto');
                }
                if (waypointsCompleto[i]['tipo_punto'] == 'Punto de control') {
                  makeMarker(waypoints[i]['location'], icons.puntoControl, 'Punto de control');
                }if (waypointsCompleto[i]['tipo_punto'] == 'Punto normal') {
                } 
              }
              for (var i = participantes.length - 3; i < participantes.length; i++) {
                makeMarker(participantes[i], icons.jugador, 'Participante');
              }
            }
            else{
              window.alert('Directions request failed due to ' + status);
            }
          
          
          });

      }
function makeMarker(position, icon, title) { 
 
    new google.maps.Marker({
      position: position, 
      map: map, 
      icon: icon, 
      title: title 
    });
}
      crearRuta();
    }

    function displayPathElevation(path, elevator, mapElevation) {

        elevator.getElevationAlongPath({
          'path': path,
          'samples': 256
        }, plotElevation);
      }
      
function plotElevation(elevations, status) {
  var chartDiv = document.getElementById('elevation_chart');
  if (status !== 'OK') {
    chartDiv.innerHTML = 'Cannot show elevation: request failed because '+
    status;
    return;
  }
  var chart = new google.visualization.ColumnChart(chartDiv);
  google.visualization.events.addListener(chart, 'onmouseover', verInfoOver);
  google.visualization.events.addListener(chart, 'onmouseout', verInfoOut);
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Sample');
  data.addColumn('number', 'Elevation');
  for (var i = 0; i < elevations.length; i++) {
    var ele = Math.round(elevations[i].elevation);
    data.addRow(['', ele]);
  }
  function verInfoOver(e){
    chart.setSelection([e]);
      }
  
  function verInfoOut(e){
    chart.setSelection([{'row': null, 'column': null}]);
  }
  chart.draw(data, {
    height: 150,
    legend: 'none',
    titleY: 'Elevation (m)'
  });
}

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9y3PjQzU5P5cFGGDzD8dqZi_6WfGvtfQ&callback=initMap"
  type="text/javascript"></script>



            @endif
            @isset($tiempos)

            {{-- Grafico de tiempos --}}
            <script>
            require(['c3', 'jquery'], function(c3, $) {
              $(document).ready(function(){
                var chart = c3.generate({
                  bindto: '#chart-temperature', // id of chart wrapper
                  data: {
                    x: 'x',            
                    columns: [
                        // each columns data
                      ['x',0,@for ($i = 0; $i < count($puntosControl); $i++){{$puntosControl[$i]['km']}}, @endfor],
                      ['data1', 0,@for ($i = 0; $i < count($min); $i++){{$min[$i]}}, @endfor ],
                      
                    ],
                    labels: true,
                    type: 'line', // default type of chart
                    colors: { 
                      'data1': tabler.colors["blue"],
                    },
                    names: {
                      'data1': 'Minuto',
                    }
                  },
                  axis: {
                    x: {
                      label: {
                      text: 'Kms',
                      position: 'outer-center'
                    }
                    },
                    y: {
                    label: {
                      text: 'Minutos',
                      position: 'outer-middle'
                    }
                  },
                  },
                  legend: {
                            show: false, //hide legend
                  },
                  padding: {
                    bottom: 0,
                    top: 0
                  },
                });
              });
            });
            </script>
            @endisset
      </div>
    </div>   
  </div>


</div>



<div class="logos">
   
     
        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:100px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:100px;overflow:hidden;">

                @foreach($patrocinadores as $patrocinador) 
                      <div>

                            <a @php echo ($patrocinador['url']!='')? 'target="_blank" href="'.$patrocinador['url'].'"': 'href="#"'; @endphp>
                              <img data-u="image" src="{{asset('/img/'.$patrocinador['img'] )}}" alt="{{ $patrocinador['nombre'] }}"/>
                            </a>
                      </div>
                 
                @endforeach
                @foreach($patrocinadores as $patrocinador) 
                      <div>

                            <a @php echo ($patrocinador['url']!='')? 'target="_blank" href="'.$patrocinador['url'].'"': 'href="#"'; @endphp>
                              <img data-u="image" src="{{asset('/img/'.$patrocinador['img'] )}}" alt="{{ $patrocinador['nombre'] }}"/>
                            </a>
                      </div>
                 
                @endforeach
      </div>
    </div>



</div>


<script src="{{ asset('/js/jssor.slider.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {
            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 0,
              $SlideDuration: 5000,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Align: 0
            };
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            /*#region responsive code begin*/
            var MAX_WIDTH = 980;
            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;
                if (containerWidth) {
                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <script type="text/javascript">jssor_1_slider_init();</script>
@endsection