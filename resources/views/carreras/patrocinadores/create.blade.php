@extends('layouts.template')
@section('content')



 <div class="my-3 my-md-5">
	    <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Crear patrocinador
              </h1>
            </div>
            <div class="col-lg-12">

              <form class="card" method="POST" action="{{route('patrocinadores.store')}}" enctype="multipart/form-data">
              	 @csrf
<input type="hidden" value="{{ $carrera->id }}" name="carrera">
                <div class="card-body">
                  <h3 class="card-title">Crear patrocinador</h3>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="form-label">Nombre del patrocinador</label>
                        <input type="text" class="form-control" placeholder="Nombre del patrocinador" value="" name="nombre">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                      <div class="form-group">
                        <label class="form-label">Url de página del patrocinador</label>
                        <input type="text" class="form-control" placeholder="Url de página del patrocinador" value="" name="url">
                      </div>
                    </div>


                    <div class="col-md-12">
                     
                        <div class="form-group">
                        <div class="form-label">Patrocinador principal</div>
                        <div class="custom-controls-stacked">
                          {{-- <label class="custom-control custom-checkbox">
                            <input class="custom-control-input" name="principal" value="" checked="" type="checkbox">
                            <span class="custom-control-label">Este es el principal?</span>
                          </label> --}}

                          <input  name="principal"  type="checkbox" value="true">
                        </div>
                      </div>

                    </div>
                    <div class="form-group col-md-12">
                        <div class="form-label">Subir logo de patrocinador</div>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="img" onchange="readURL(this);">
                          <label class="custom-file-label">Elegir imagen</label>
                        </div>
                        <img id="blah" src="#" alt="Logo"  />
                    </div>
                  </div>
                </div>
			</div>



                <div class="card-footer text-right">
                  <a href="{{ url('/patrocinadores/'.$carrera->id) }}" class="btn btn-danger">Atras</a>  
                  <button type="submit" class="btn btn-primary">Crear patrocinador</button>
                </div>
              </form>
        </div>
    </div>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endsection