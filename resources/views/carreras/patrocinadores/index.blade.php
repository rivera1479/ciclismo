@extends('layouts.template')
@section('content')
@include('alerts.messages')


 <div class="my-3 my-md-5">
        <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Patrocinadores
              </h1>
            </div>
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Patrocinadores</h3>
                  </div>
                  <div>
                    <a href="{{ url('/carreras') }}" class="btn btn-danger">Atras</a>  
                    <a href="{{ asset('patrocinadores/create/'.$carrera->id) }}" class=" btn btn-primary">Crear patrocinador</a>
                  </div>
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Carreras</h3>
                  </div>
                  <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap">
                      <thead>
                        <tr>
                          <th class="w-1">Patrocinador</th>
                          <th>Logo</th>
                          <th>URL</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($patrocinadores as $patrocinador)
                        <tr>
                          <td><span class="text-muted">{{ $patrocinador['nombre'] }}</span></td>
                          <td class="text-center"><img src="{{ asset('/img/'.$patrocinador['img']) }}" alt="" style="height: 80px"></td>
                          <td>{{ $patrocinador['url'] }}</td>
                          <td class="text-right">
                            <a class="icon" href="{{ url('/patrocinadores/edit',$patrocinador['id'])}}"><i class="fe fe-edit"></i></a>
                            <form action="{{ route('patrocinadores.destroy',$patrocinador['id'])}}" method="POST">
                              @csrf
                              {{ method_field('DELETE') }}
                              
                              <button class="fa fa-trash icon"></button>
                            </form>
                          </td>

                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                     <center>  {{ $patrocinadores->links() }}</center>
                  </div>
                </div>
              </div>
                  


                </div>
              </div>
    </div>
</div>
    <script></script>
@endsection