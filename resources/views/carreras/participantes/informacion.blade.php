@extends('layouts.template')
@section('content')
@include('alerts.messages')


<div class="my-3 my-md-5">
  <div class="container">
    <div class="page-header">
      <h1 class="page-title">
       Información de participante
     </h1>
   </div>
   <div class="col-12">
    <div class="card">
      <div class="col-4">
        <strong> Nombre completo: </strong>{{ $participante['nombre'] }} {{ $participante['apellido'] }} 
      </div>
    <div class="col-4">
      <strong>DNI:</strong> {{ $participante['dni']  }}
    </div>

    <div class="col-4">
      <strong>Numero dorsal:</strong> {{ $participante['num_dorsal'] }}
    </div>
    <div class="col-4">
      <strong>Dirección:</strong> {{ $participante['direccion'] }}
    </div>

    <div class="col-4">
      <strong>Telefono:</strong> {{ $participante['telefono'] }}
    </div>
</div>
  </div>
    <div class="col-12">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Ubicaciones</h3>
      
    </div>
  <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
      <thead>
        <tr>
          <th>Latitud</th>
          <th>Longitud</th>
          <th>Fecha y hora</th>
        </tr>
      </thead>
      <tbody>
      @foreach($ubicacion as $ubi)
        <tr>        
          <td>{{ $ubi['latitud']}}</td>
          <td>{{ $ubi['longitud'] }}</td>
          <td>{{$ubi['fecha_competencia']}}</td>
        </tr>
      @endforeach
      </tbody>    
    </table>  
  </div>
<center>  {{ $ubicacion->links() }}</center>

  </div>
</div>

      <div class="card-footer text-right">
        <a href="{{ asset('/ciclistas/'.$carrera->id) }}" class="btn btn-danger">Atras</a>
        <button type="submit" class="btn btn-primary">Editar</button>
      </div>




</div>
</div>


@endsection