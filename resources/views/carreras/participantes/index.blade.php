@extends('layouts.template')
@section('content')
@include('alerts.messages')


<div class="my-3 my-md-5">
  <div class="container">
    <div class="page-header">
      <h1 class="page-title">
       Ciclistas participantes
     </h1>
   </div>
   <div class="col-12">
    <div class="card">
      <input type="hidden" id="carrera_id" value="{{ $carrera->id }}">

      <div class="col-4">
        <a href="{{ url('/carreras') }}" class="btn btn-danger">Atras</a>  
        <a href="javascript:void(0);" class=" btn btn-primary" id="ReiniciarHistorico" onclick="condiciones();">Reinicar ubicaciones</a>
        <a href="{{ asset('ciclistas/create/'.$carrera->id) }}" class=" btn btn-primary">Crear participante</a>
      </div>
      <form action="{{ url('ciclistas/'.$carrera->id)}}" method="get" class="">
        <div class="input-group col-4 ml-auto">
          <input type="text" class="form-control" placeholder="Buscar por dorsal" name="num_dorsal">
          <span class="input-group-append">
            <button class="btn btn-primary" type="button">Buscar</button>
          </span>
        </div>
      </form>
      @if(count($participantes) == 0)
      <h2>No hay participantes inscritos</h2>
      @else

      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Ciclistas</h3>
          </div>
          <div class="table-responsive">
            <table class="table card-table table-vcenter text-nowrap">
              <thead>
                <tr>
                  <th class="w-1">Nombre</th>
                  <th>Apellido</th>
                  <th>DNI</th>
                  <th>Dorsal</th>
                  <th>Telefono </th>
                  <th>Direccion</th>
                  <th>User app</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($participantes as $participante)
                <tr>
                  <td><span class="text-muted">{{ $participante['nombre'] }}</span></td>
                  <td>{{ $participante['apellido'] }}</td>

                  <td>{{ $participante['dni'] }}</td>
                  <td>{{ $participante['num_dorsal'] }}</td>
                  <td>{{ $participante['telefono'] }}</td>
                  <td>{{ $participante['direccion'] }}</td>
                  <td>{{ $participante->users()->get()->last()->username }}</td>
                  <td class="text-right">
                    <a class="icon" href="{{ url('/ciclistas/show',$participante['id'])}}"><i class="fe fe-info"></i></a>
                    <a class="icon" href="{{ url('/ciclistas/edit',$participante['id'])}}"><i class="fe fe-edit"></i></a>
                    <form action="{{ route('ciclistas.destroy',$participante['id'])}}" method="POST">
                      @csrf
                      {{ method_field('DELETE') }}

                      <button class="fa fa-trash icon"></button>
                    </form>
                  </td>

                </tr>
                @endforeach

              </tbody>
            </table>
            <center>  {{ $participantes->links() }}</center>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</div>
</div>
<script>
  function condiciones()
  {
    var opcion = confirm("Esta seguro que desea reinciar el historico de ubicaciones? En ese caso borrar todas las ubicaciones que esten registradas en esta carrera.");
    if (opcion == true) {
      guardar();
    } else {
    }
  }


  function guardar(){
    var carrera = $("#carrera_id").val();
    $.ajax({
      url:  $('#url_base').val()+'/ciclistas/reiniciarHistorico',
      type: 'POST',
      data: {
        '_token': $('input[name=_token]').val(),
        'carrera': carrera
      },

      success: function(data)
      {
        if ((data.success==true))
        {
          alert('Se borraron todas las ubicaciones');
        }
        else{
          alert('Ocurrio algun problema');
        }

      }
    });
  }
</script>
@endsection