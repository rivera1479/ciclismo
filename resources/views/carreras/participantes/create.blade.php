@extends('layouts.template')
@section('content')
@include('alerts.messages')


<div class="my-3 my-md-5">
  <div class="container">
    <div class="page-header">
      <h1 class="page-title">
       Crear participante
     </h1>
   </div>
   <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Formulario</h3>
      </div>

      <div class="col-lg-12">
        <form class="card" method="POST" action="{{ url('ciclistas/store') }}">
          <input type="hidden" value="{{ $carrera->id }}" name="carrera">
         @csrf
         <div class="card-body">
          <h3 class="card-title">Crear participante</h3>
          <div class="row">
            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Nombre</label>
                <input type="text" class="form-control" placeholder="" value="" name="nombre">
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Apellido</label>
                <input type="text" class="form-control" placeholder="" value="" name="apellido">
              </div>
            </div>

            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">DNI</label>
                <input type="text" class="form-control" placeholder="" value="" name="dni">
              </div>
            </div>

            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Dorsal</label>
                <input type="text" class="form-control" placeholder="" value="" name="num_dorsal">
              </div>
            </div>

            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Teléfono</label>
                <input type="text" class="form-control" placeholder="" value="" name="telefono">
              </div>
            </div>

            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Dirección</label>
                <input type="text" class="form-control" placeholder="" value="" name="direccion">
              </div>
            </div>

            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Contraseña</label>
                <input type="password" class="form-control password1" placeholder="" value="" name="password">
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Repetir Contraseña</label>
                <input type="password" class="form-control password2" placeholder="" value="" name="password">
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="card-footer text-right">
        <a href="{{ asset('/ciclistas/'.$carrera->id) }}" class="btn btn-danger">Atras</a>
        <button type="submit" class="btn btn-primary btnPassword">Crear participante</button>
      </div>
    </form>


  </div>
</div>
</div>
</div>


@endsection