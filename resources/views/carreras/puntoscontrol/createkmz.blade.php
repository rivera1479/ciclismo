@extends('layouts.template')
@section('content')
@include('alerts.messages')
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 30px;
        left: 75%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
    </style>

<div class="my-3 my-md-5">
  <div class="container">
    <div class="row row-cards"> 
      <div class="col-lg-12 col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Suba el archivo kmz</h3>
          </div>
          <div class="card-body">
          
              <form action="{{route('routekmz.store',['carrera_id'=>$carrera->id])}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="file" name="archivo">

                <div class="card-footer text-right">
                  <a href="{{ url('/punto/'.$carrera['id']) }}" class="btn btn-danger p-2">Atras</a>  
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>

@endsection