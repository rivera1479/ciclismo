@extends('layouts.template')
@section('content')
@include('alerts.messages')


<div class="my-3 my-md-5">
  <div class="container">
    <div class="page-header">
      <h1 class="page-title">
       Editar punto
     </h1>
   </div>
   <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Formulario</h3>
      </div>

      <div class="col-lg-12">
        <form class="card" method="POST" action="{{ url('punto/update') }}">
          <input type="hidden" value="{{ $carrera->id }}" name="carrera">
          <input type="hidden" value="{{ $puntocontrol->id }}" name="puntocontrol">
         @csrf
         <input type="hidden" value="" name="url">
         <input type="hidden" value="" name="fecha">
         <div class="card-body">
          <h3 class="card-title">Editar puntos</h3>
          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label class="form-label">Titulo</label>
                <input type="text" class="form-control" placeholder="titulo" value="{{ $puntocontrol->titulo }}" name="titulo">
              </div>
            </div>
                <input type="hidden" class="form-control" placeholder="" value="{{ $puntocontrol->km }}" name="km">
            <div class="col-sm-6 col-md-3">
              <div class="form-group">
                <label class="form-label">Tipo de control</label>
                <select name="tipo_punto" id="tipo_punto" class="form-control custom-select">
                  @foreach($tipo as $key => $value)
                    @if($value == $puntocontrol->tipo_punto)
                      <option value="{{$value}}" selected="">{{$value}}</option>
                    @else
                    <option value="{{$value}}">{{$value}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="card-footer text-right">
        <a href="{{ url('/punto/'.$carrera->id) }}" class="btn btn-danger">Atras</a>  
        <button type="submit" class="btn btn-primary">Editar carrera</button>
      </div>
    </form>


  </div>
</div>
</div>
</div>
<script>
  
</script>
@endsection