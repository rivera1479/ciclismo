@extends('layouts.template')
@section('content')

<div class="alert alert-success alert-dismissible" role="alert" style="display: none" id="mensaje-success">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="alert alert-danger alert-dismissible" role="alert" style="display: none" id="mensaje-error">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>

<div class="my-3 my-md-5">
 <div class="container">
    <div class="porcentaje" style="height: 40px; width: 400px;" >
    <div id="terminado" style="background: rgb(40, 167, 69); width: 0%;height: 40px;"></div>
    <div id="falta" style="width: 100%; height: 40px"></div>
  </div>
  <div class="page-header">
    <h1 class="page-title">
     Importar tiempos de participantes
   </h1>
 </div>
 <div class="col-lg-12">

  
    <input type="hidden" value="{{$carrera['id']}}" name="carrera_id" id="carrera_id">
    @csrf
    <div class="form-group col-md-6">
      <div class="form-label">Importar archivo</div>
      <div class="custom-file">
        <input type="file" class="custom-file-input" name="file" id="files">
        <label class="custom-file-label">Elegir archivo</label>
      </div>
      <div id="mensajeError"></div>


    </div>

    <div class="card-footer text-right">

      <form action="{{url('export/')}}" enctype="multipart/form-data" method="POST">
        @csrf
      <a href="{{ asset('/punto/'.$carrera['id']) }}" class="btn btn-danger">Atras</a>
      <a href="javascript:void(0)" class="btn btn-primary" id="submit-file">Subir archivo</a>
      <input type="hidden" value="{{$carrera['id']}}" name="id_carrera" id="id_carrera">
      <input type="hidden" value="{{$puntocontrol['id']}}" name="punto_control" id="punto_control">
    </form>
    </div>

</div>
</div>
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>  --}}
  
  <script src="{{ asset('/js/papaparse/papaparse.min.js') }}" type="text/javascript"></script>

  <script>
        $(document).ready(function(){
          $(".porcentaje").hide();
    });
    $('#submit-file').on("click",function(e){
      e.preventDefault();
      $('#files').parse({
        config: {
          delimiter: "auto",
          complete: completeFn,
        },
        before: function(file, inputElem)
        {
            console.log("Parsing file...", file);
        },
        error: function(err, file)
        {
            console.log("ERROR:", err, file);
        },
        complete: function()
        {
            console.log("Done with all files");
        }
    });
});

    function completeFn(results)
    {
      var data = results.data;
      var carrera_id = $("#carrera_id").val();
      var punto_control = $("#punto_control").val();

      var count = 0;
      for (var i = 0; i < data.length - 1; i++) {
        var row = data[i];
        if (row[0] != '') {
          datarow = row[0];
          rowarray = datarow.split(",");
          patron = /"/g;
          num_dorsal = rowarray[0];

          $.ajax({
            url:$('#url_base').val()+'/guardarCsvTiempos',
            type:'POST',
            data:{
              _token: '{{csrf_token()}}',
              carrera_id,
              punto_control: punto_control,
              datajson:datarow},
            })
          .done(function( msg ) {
            $(".porcentaje").show();
            count++;
            var porcentaje = count*100/i;
            $('#terminado').css('width', porcentaje+'%');
            $('#falta').css('width', (100-porcentaje)+'%');
          })
          .fail(function( data ) {
          });
        }
      }
     
    }
</script>
  
@endsection