@extends('layouts.template')
@section('content')
@include('alerts.messages')


<div class="my-3 my-md-5">
  <div class="container">
    <div class="row align-items-center">
      <div class="d-flex flex-row-reverse col-lg-12">
        <a href="{{ asset('punto/create/'.$carrera->id) }}" class=" btn btn-primary col-lg-2 p-2">
        @if(count($puntosControl) > 1)
          Reiniciar ruta manual       
        @else
        Crear ruta manual
        @endif
      </a>
        <a href="{{ asset('punto/createkmz/'.$carrera->id) }}" class=" btn btn-primary col-lg-2 p-2">
        @if(count($puntosControl) > 1)
          Reiniciar ruta manual kmz        
        @else
        Crear ruta kmz
        @endif
      </a>
        <a href="{{ url('/carreras') }}" class="btn btn-danger p-2">Atras</a>  
        
      </div>
    </div>
   <div class="row row-cards">

      
@if(count($puntosControl) > 1)
  @if($puntosControl[0]['tipo_punto'] == 'KMZ')
    <div class="col-12 col-lg-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Ruta de carrera</h3>
        </div>
        <div class="card-body">
          <div id="map" class="map"></div>
        </div>
      </div>
    </div>
  @else
    <div class="col-12 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Ruta de carrera</h3>
        </div>
        <div class="card-body">
          <div id="map" class="map"></div>
        </div>
      </div>
    </div>
    
    <div class="col-12 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Puntos</h3>
        </div>
        <div class="table-responsive">
          <table class="table card-table table-vcenter text-nowrap">
            <thead>
              <tr>
                <th class="w-1">Titulo</th>
                <th>KM</th>
                <th>Tipo de control</th>
                <th>Acciones</th>
              </tr>
            </thead>
            @php $distancia = 0; @endphp
            <tbody>
              @foreach($puntosControl as $puntos)
              <tr>
                <td><span class="text-muted">{{ $puntos['titulo'] }}</span></td>
                @php $distancia = $distancia + $puntos['km'] ; @endphp
                <td>{{ $distancia }}</td>
                <td>{{$puntos['tipo_punto']}}</td>
                <td class="text-right">
                  @if($puntos['tipo_punto'] == 'Punto de control')
                  <a class="icon" href="{{ url('/punto/cargaform',$puntos['id'])}}" title="Cargar tiempos"><i class="fe fe-file"></i></a>
                  @endif
                  <a class="icon" href="{{ url('/punto/edit',$puntos['id'])}}"><i class="fe fe-edit"></i></a>
                </td>

              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  @endif
@else
    <h1>No hay ruta registrada, por favor registre una ruta</h1>
@endif
  </div>
</div>
</div>
<script>
 function initMap() { 
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});


@empty($puntosControl)
@else        
@if($puntosControl[0]['tipo_punto'] == 'KMZ')
        var rutaCoordinates =[
        @foreach($puntosControl as $punto)

        {lat:{{$punto['latitud']}},lng:{{$punto['longitud']}}},
        @endforeach
        ];
var rutaMedia = parseInt(rutaCoordinates.length / 2);

        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var labelIndex = 0;
        var map = new google.maps.Map(document.getElementById('map'),{
          zoom: 10,
          center: rutaCoordinates[rutaMedia]
        });


        var rutaPath = new google.maps.Polyline({
          path: rutaCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        rutaPath.setMap(map);
@endif
@endempty
@isset($origen)


        //Ubicacion para crear un evento en una marca
        var myLatlng = {lat: 40.94082477819901, lng: -3.995340561054263};
        //Markar y etiquetar
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var labelIndex = 0;
        var map = new google.maps.Map(document.getElementById('map'),{
          zoom: 12,
          center: myLatlng
        });

directionsDisplay.setMap(map);
        function crearRuta() {

          var icons = {
            start: new google.maps.MarkerImage(
            // URL 
            '{{asset('images/inicio.png')}}',
            // (width,height) 
            new google.maps.Size(44, 32),
            // The origin point (x,y)
            new google.maps.Point(0, 0), 
            // The anchor point (x,y) 
            new google.maps.Point(22, 32) 
            ), 
            end: new google.maps.MarkerImage(
            // URL
            '{{asset('images/finish.png')}}',
            // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
    ),
    jugador: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/participante.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
    ),
    puerto: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/marina-2.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
),
    puntoControl: new google.maps.MarkerImage(
    // URL 
    '{{asset('images/clock.png')}}', 
    // (width,height) 
    new google.maps.Size(44, 32), 
    // The origin point (x,y) 
    new google.maps.Point(0, 0), 
    // The anchor point (x,y) 
    new google.maps.Point(22, 32) 
),};

          var ori = "{{$origen}}";
          var des = "{{$destino}}";
          var origenArray = ori.split(",");
          var destinoArray = des.split(",");
          var waypoints = [];
          var waypointsCompleto = [];

            var origen = new google.maps.LatLng(parseFloat(origenArray[0]),parseFloat(origenArray[1]));
            var destino =  new google.maps.LatLng(parseFloat(destinoArray[0]),parseFloat(destinoArray[1]));
            @foreach($arrayLatLon as $key => $value)
              var way = new google.maps.LatLng(parseFloat( {{$value['lat']}}),parseFloat( {{$value['lon']}}));
              var tipo_punto = "{{$value['tipo_punto']}}";
              var titulo = "{{$value['titulo']}}";
              waypoints.push({
                location:way,
                stopover: true});

              waypointsCompleto.push({
                tipo_punto : tipo_punto,
                titulo : titulo
              });

            @endforeach

          directionsService.route({
            origin:  origen ,
            destination: destino,
            waypoints:  waypoints,
            travelMode: 'BICYCLING'
          }, function(response, status){
            if(response, status){              
              directionsDisplay.setDirections(response);
              var leg = response.routes[ 0 ].legs[ 0 ]; 
              makeMarker(leg.start_location, icons.start, "Inicio de carrera"); 
              makeMarker(destino, icons.end, 'Final de punto');
              for (var i = 0; i < waypoints.length; i++) {
                if (waypointsCompleto[i]['tipo_punto'] == 'Puerto') {
                  makeMarker(waypoints[i]['location'], icons.puerto, waypointsCompleto[i]['titulo']);
                }
                if (waypointsCompleto[i]['tipo_punto'] == 'Punto de control') {
                  makeMarker(waypoints[i]['location'], icons.puntoControl, waypointsCompleto[i]['titulo']);
                }if (waypointsCompleto[i]['tipo_punto'] == 'Punto normal') {

                }
                
              }
            }
            else{
              window.alert('Directions request failed due to ' + status);
            }
          
          
          });
        }
        function makeMarker(position, icon, title) { 
          var markerInfo = new google.maps.Marker({ 
            position: position, 
            map: map, 
            icon: icon, 
            title: title 
          });

          var stepDisplay = new google.maps.InfoWindow;
          google.maps.event.addListener(markerInfo, 'click', function(){
            var text = 'Texto de informacion';
            stepDisplay.setContent(title);
            stepDisplay.open(map, markerInfo);
          });
        }
        crearRuta();


@endisset
      }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9y3PjQzU5P5cFGGDzD8dqZi_6WfGvtfQ&callback=initMap"
    type="text/javascript"></script>
    @endsection