@extends('layouts.template')
@section('content')
@include('alerts.messages')
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 30px;
        left: 75%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
    </style>

<div class="my-3 my-md-5">
  <div class="container">
    <div class="row row-cards"> 
      <div class="col-lg-8 col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Mapa para crear puntos</h3>
          </div>
          <div class="card-body">
          <div id="floating-panel">
            <input onclick="deshacerPunto();" type=button value="Deshacer">
          </div>
            <div id="map" class="map"></div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Panel para puntos</h3>
          </div>
          <div class="card-body">
            <div class="row gutters-xs" id="campos"></div>
            <div class="card-footer text-right">
              <a href="{{ url('/punto/'.$carrera['id']) }}" class="btn btn-danger p-2">Atras</a>  
              <input type="button" class="btn btn-primary" id="guardar" value="Guardar" onclick="condiciones()">
            </div>

            <input type="hidden" value="{{$carrera->id}}" id="carrera_id">
            <input type="hidden" value="" id="locations">
            <input type="hidden" value="" id="distancia">
            
          </div>
        </div>

      </div>


    </div>
  </div>
</div>

<script>
  var map;
  var locations = [];var markers = [];
  var labelIndex = 0;
  var etiquetaIndex = 0;
  var directionsService;
  var directionsDisplay;
  var createInput = 0;
  var undoStatus = false;

  function condiciones(){
    var opcion = confirm("Esta seguro que desea reinciar la ruta? En ese caso se borrara la anterior y se creara una nueva.");
    if (opcion == true) {
      guardar();
    }else{
    }
  }

  function guardar(){
    var ubicacion = $("#locations").val();
    var inputs = [];
    var tipoControl = [];
    $(".inputs").each(function(){
      var nombrePunto=$(this).val();
      inputs.push(nombrePunto);
    });

    $(".tipoControl").each(function(){
      var tipoControlPunto=$(this).val();
      tipoControl.push(tipoControlPunto);
    });

    $.ajax({
      url:  $('#url_base').val()+'/punto/store',
      type: 'POST',
      data: {
        '_token': $('input[name=_token]').val(),
        'ubicacion': ubicacion,
        'inputs': inputs,
        'distancias': $("#distancia").val(),
        'carrera_id': $("#carrera_id").val(),
        'tipoControl': tipoControl,
      },success: function(data){
        if ((data.success==true)){
          alert('Ruta creada!');
        }else{
          alert('Ocurrio algun problema');
        }
      }
    });
  }

  function initMap(){
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay = new google.maps.DirectionsRenderer;
    var myLatlng = {lat: 40.94082477819901, lng: -3.995340561054263};
    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    labelIndex = 0;
    map = new google.maps.Map(document.getElementById('map'),{
      zoom: 12,
      center: myLatlng
    });
    directionsDisplay.setMap(map);
    google.maps.event.addListener(map, 'click', function(event) {
      locations.push(event.latLng);
      $("#locations").val(locations);
      createInput = 0;
      addMarker(event.latLng, map, locations,directionsService,directionsDisplay);
    });

    function addMarker(location, map,locations,directionsService,directionsDisplay) {
      if(locations.length == 1){
        var marker = new google.maps.Marker({
          position: location,
          label: labels[labelIndex++ % labels.length],
          map: map
        });
        markers.push(marker);
        if (createInput == 1) {  
        }else{
          crearInput();
        }
      }
      if(locations.length == 2){
        directionsService.route({
          origin: locations[0],
          destination: locations[1],
          travelMode: 'BICYCLING'
        },function(response, status){
          if(response, status){
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var distancia = [];
            if (this.undoStatus == true){
              this.undoStatus = false;
            }
            for(var i = 0;i < route.legs.length;i++){
              distancia.push(route.legs[i].distance.text);
              $("#distancia").val(distancia);
              if (createInput == 1) {  
              }else{
                crearInput();
              }
            }
          }else{
            window.alert('Directions request failed due to ' + status);
          }
        });
      }

      if (locations.length > 2) {
        var waypts = [];
        var origen = locations.slice(0,1);
        var destino = locations.slice(-1);
        var local = locations.slice(1,-1);
        for(var i = 0; i < local.length; i++){
          waypts.push({
            location: local[i]
          });
        }
        directionsService.route({
          origin: origen[0],
          destination: destino[0],
          waypoints: waypts,
          travelMode: 'BICYCLING'
        },function(response, status){
          if(response, status){
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var num = route.legs.length - 1;
            if (this.undoStatus == false){
              console.log('entro');
              var distancia = $("#distancia").val();
              distancia = distancia+','+route.legs[num].distance.text;
              $("#distancia").val(distancia);
            }else{
              console.log(undoStatus);
              this.undoStatus = false;
              console.log(undoStatus);
            }

            if (createInput == 1) {
            }else{
              crearInput();
            }

          }
          else{
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    }
    var etiquetas = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    function crearInput(){
      var num = document.getElementsByClassName('inputs').length+ 1;
      var etiqueta = etiquetas[etiquetaIndex++ % etiquetas.length];
      var padre = document.getElementById("campos");
      var divPunto = document.createElement("DIV");
      divPunto.setAttribute("class","col-8");
      divPunto.setAttribute("id","divPunto_"+num);
      padre.appendChild(divPunto);

      var nombrePunto = document.createElement("INPUT");   
      nombrePunto.setAttribute("type", "text");
      nombrePunto.setAttribute("name", "name_"+num);
      nombrePunto.setAttribute("class", "inputs form-control");
      nombrePunto.setAttribute("placeholder", "Punto "+etiqueta);
      nombrePunto.setAttribute("value", "Punto "+etiqueta);
      document.getElementById("divPunto_"+num).appendChild(nombrePunto);

      var divTipoPunto = document.createElement("DIV");
      divTipoPunto.setAttribute("class", "col-4");
      divTipoPunto.setAttribute("id","divTipoPunto_"+num);
      padre.appendChild(divTipoPunto);

      var tipoPunto = document.createElement("SELECT");
      tipoPunto.setAttribute("class","form-control custom-select tipoControl");
      tipoPunto.setAttribute("name", "tipoControl_"+num);
      tipoPunto.setAttribute("id", "selecTipo_"+num);
      document.getElementById("divTipoPunto_"+num).appendChild(tipoPunto);

      var option1 = document.createElement("option");
      option1.setAttribute("value","Punto normal");
      option1.setAttribute("selected","true");
      var textOption1 = document.createTextNode("Punto normal");
      option1.appendChild(textOption1);
      document.getElementById("selecTipo_"+num).appendChild(option1);

      var option2 = document.createElement("option");
      option2.setAttribute("value","Puerto");
      var textOption2 = document.createTextNode("Puerto");
      option2.appendChild(textOption2);
      document.getElementById("selecTipo_"+num).appendChild(option2);   

      var option3 = document.createElement("option");
      option3.setAttribute("value","Punto de control");
      var textOption3 = document.createTextNode("Punto de control");
      option3.appendChild(textOption3);
      document.getElementById("selecTipo_"+num).appendChild(option3);
    }

    return addMarker;
  }

  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  function clearMarkers() {
    setMapOnAll(null);
  }

  function deshacerPunto(){
    undoStatus = true;

    var distancia = $("#distancia").val();
    var arrayDistancia = distancia.split("km,");
    var arrayFinal = [];
    arrayDistancia.pop();
    for (var i = 0; i < arrayDistancia.length; i++) {
      if (arrayDistancia.length - 1 == i){
        arrayFinal = arrayFinal+arrayDistancia[i]+'km'; 
      }else{
        arrayFinal = arrayFinal+arrayDistancia[i]+'km,';
      }
    }
    $("#distancia").val(arrayFinal);
    var totalLocations = locations.length;
    if (totalLocations == 1) {
      clearMarkers();
      markers = [];
      locations = [];
      labelIndex = 0;
      etiquetaIndex = 0;
      $(".inputs").parent('div').remove();
      $(".tipoControl").parent('div').remove();
    }

    if (totalLocations > 1) {
      locations.pop();
      markers.pop();
      var totalInput = $(".inputs").length;
      $("#divTipoPunto_"+totalInput).remove();
      $("#divPunto_"+totalInput).remove();
      labelIndex = labelIndex - 1;
      etiquetaIndex = etiquetaIndex - 1;
      createInput = 1;
      if (totalLocations == 2) {
        initMap()(locations[0], map, locations,directionsService,directionsDisplay);  
      }else{
        var local = totalLocations - 1;
        initMap()(locations[local], map, locations,directionsService,directionsDisplay);  
      }
    }

  }
</script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9y3PjQzU5P5cFGGDzD8dqZi_6WfGvtfQ&callback=initMap">
</script>
@endsection