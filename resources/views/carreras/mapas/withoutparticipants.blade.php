<div class="col-12 text-center">
  <div class="card">
    <div class="card-header" style="display: block;border:0">
      <h3 class="card-title titulo">RUTA DE CARRERA</h3>
    </div>
    <div class="card-body">
      <div id="map" class="map"></div>
      <div id="elevation_chart" class="c3"></div>
    </div>
  </div>
</div>
@isset($mensajebusqueda)
<div class="col-12 text-center">
<h3>{{$mensajebusqueda}}</h3>
</div>
@endisset
<div class="col-12" id="buscador">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Buscar por dorsal o telefono</h3>
        </div>
        <div class="card-body">
          <div class="input-group mb-3" >    
            <input type="search" id="numero" class="form-control header-search" placeholder="Buscar" tabindex="1" onkeypress="return enter(event)">
            <input type="hidden" value="{{$race['url']}}" id="carreraurl">
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="submit" onclick="buscarNum();"><i class="fe fe-search"></i></button>
            </div>
          </div>
        </div>
      </div>
    </div>
<script>
  function enter(e){
    if (e.keyCode == 13) {
        buscarNum();
    }

  }
  function buscarNum(){
    var num = $("#numero").val();
    var url = $("#carreraurl").val();
    var url_base = $('#url_base').val();
          $.ajax({
            url:url_base+'/buscar/participante',
            type: 'POST',
            data:{
              '_token': $('input[name=_token]').val(),
              'num': num,
              'url': url
            },success:function(data){
              if ((data.success==true)) {
                window.location.href = url_base+'/carreras/'+data.url+'/'+data.num_dorsal ;
                console.log('Si hay');
              }else{
                alert(data.error);
              }
            }
          });
        }

</script>

{{-- Script para hacer ruta --}}
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>

  google.charts.load("current", {packages:['corechart']});

  var map;
  var mousemarker;
  var arraypunto = [];
  var carrera_id;
  var controlPunto = [];



function initMap(){
  carrera_id = "{{$race['id']}}";
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
  var elevator = new google.maps.ElevationService;

  @foreach($puntosControl as $punto)
    @if($punto['tipo_punto'] == 'Punto de control' or $punto['tipo_punto'] == 'Puerto')
      var latPunto = '{{$punto['latitud']}}';
      var lngPunto = '{{$punto['longitud']}}';
      var tipo_punto  = '{{$punto['tipo_punto']}}';
      controlPunto.push({
        lat:latPunto,
        lng:lngPunto,
        tipo: tipo_punto
      });
    @endif

  @endforeach

  @if($puntosControl[0]['tipo_punto'] == 'KMZ')

        var rutaCoordinates =[
        @foreach($puntosControl as $punto)

        {lat:{{$punto['latitud']}},lng:{{$punto['longitud']}}},
        @endforeach
        ];
var rutaMedia = parseInt(rutaCoordinates.length / 2);

        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var labelIndex = 0;
        var map = new google.maps.Map(document.getElementById('map'),{
          zoom: 10,
          center: rutaCoordinates[rutaMedia]
        });


        var rutaPath = new google.maps.Polyline({
          path: rutaCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        rutaPath.setMap(map);

        elevator.getElevationAlongPath({
          'path': rutaCoordinates,
          'samples': 256
        }, plotElevation);
      


  @else


  var myLatlng = {lat: 40.94082477819901, lng: -3.995340561054263};
  var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var labelIndex = 0;
  var map = new google.maps.Map(document.getElementById('map'),{
    zoom: 12,
    center: myLatlng,
    mapTypeId: 'terrain'
  });



  @if(count($latLonUbi) > 0)
  @if($race->status == 1)
  cluster(map);
  @endif
  @endif




  directionsDisplay.setMap(map);
  
 crearRuta();


  function crearRuta() {
    var icons = {
      start: new google.maps.MarkerImage(
        '{{asset('images/inicio.png')}}',
        new google.maps.Size(44, 32),
        new google.maps.Point(0, 0),
        new google.maps.Point(22, 32)
        ),
      end: new google.maps.MarkerImage(
        '{{asset('images/finish.png')}}',
        new google.maps.Size(44, 32),
        new google.maps.Point(0, 0),
        new google.maps.Point(22, 32)
        ),
      jugador: new google.maps.MarkerImage(
        '{{asset('images/participante.png')}}',
        new google.maps.Size(44, 32),
        new google.maps.Point(0, 0),
        new google.maps.Point(22, 32)
        ),
      puerto: new google.maps.MarkerImage(
        '{{asset('images/marina-2.png')}}',
        new google.maps.Size(44, 32),
        new google.maps.Point(0, 0),
        new google.maps.Point(22, 32)
        ),
      puntoControl: new google.maps.MarkerImage(
        '{{asset('images/clock.png')}}',
        new google.maps.Size(44, 32),
        new google.maps.Point(0, 0),
        new google.maps.Point(22, 32)
        ),
    };

    var ori = "{{$origen}}";
    var des = "{{$destino}}";
    var origenArray = ori.split(",");
    var destinoArray = des.split(",");
    var waypointsCompleto = [];
    var waypoints = [];


    var origen = new google.maps.LatLng(parseFloat(origenArray[0]),parseFloat(origenArray[1]));
    var destino =  new google.maps.LatLng(parseFloat(destinoArray[0]),parseFloat(destinoArray[1]));  
    @foreach($arrayLatLon as $key => $value)
    var way = new google.maps.LatLng(parseFloat( {{$value['lat']}}),parseFloat( {{$value['lon']}}));
    var tipo_punto = "{{$value['tipo_punto']}}";
    var titulo = "{{$value['titulo']}}";
    var km = "{{$value['km']}}";
    waypoints.push({
      location:way,
      stopover: true});

    waypointsCompleto.push({
      tipo_punto : tipo_punto,
      titulo: titulo,
      km: km
    });
    @endforeach

    directionsService.route({
      origin:  origen,
      destination: destino,
      waypoints:  waypoints,
      travelMode: 'BICYCLING'
    }, function(response, status){
      if(response, status){
        directionsDisplay.setDirections(response);
        elevator.getElevationAlongPath({
          path: response.routes[0].overview_path,
          samples: 256
        }, plotElevation);
        var leg = response.routes[0].legs[0];
        makeMarker(leg.start_location, icons.start, "Inicio de punto"); 
        makeMarker(destino, icons.end, 'Final de punto');
        var km_total = 0;
        for (var i = 0; i < waypoints.length; i++) {
          var km = parseInt(waypointsCompleto[i]['km']);
          km_total = km_total + km;
          if (waypointsCompleto[i]['tipo_punto'] == 'Puerto') {
            makeMarker(waypoints[i]['location'], icons.puerto,waypointsCompleto[i]['titulo'],km_total+" KM");
          }
          if (waypointsCompleto[i]['tipo_punto'] == 'Punto de control') {
            
            arraypunto.push({puntos:waypoints[i]});
            var location = waypoints[i]['location'];
            makeMarker(waypoints[i]['location'], icons.puntoControl,waypointsCompleto[i]['titulo'],km_total+" KM");
          }if (waypointsCompleto[i]['tipo_punto'] == 'Punto normal') {
          }
        }
      }
      else{
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  function makeMarker(position, icon, title,km = "") {
    var markerInfo = new google.maps.Marker({
      position: position,
      map: map,
      icon: icon,
      title: title
    });

    var stepDisplay = new google.maps.InfoWindow;
    google.maps.event.addListener(markerInfo, 'click', function(){
      var text = "<p>"+title+"</p> <p>"+km+"</p>";
      stepDisplay.setContent(text);
      stepDisplay.open(map, markerInfo);
    });
  }

 


  @endif
  function plotElevation(elevations, status) {



    var chartDiv = document.getElementById('elevation_chart');
    if (status !== 'OK') {
      chartDiv.innerHTML = 'Cannot show elevation: request failed because '+
      status;
      return;
    }
    var chart = new google.visualization.ColumnChart(chartDiv);
    google.visualization.events.addListener(chart, 'onmouseover', verInfoOver);
    google.visualization.events.addListener(chart, 'onmouseout', verInfoOut);
    function verInfoOver(e){
      chart.setSelection([e]);
    }
    function verInfoOut(e){
      chart.setSelection([{'row': null, 'column': null}]);
    }

    google.visualization.events.addListener(chart, 'onmouseover', function(e) {
      if (mousemarker == null) {
        mousemarker = new google.maps.Marker({
          position: elevations[e.row].location,
          map: map,
          icon: "{{asset('images/iconelevation.png')}}"
        });
      }else {
        var location = elevations[e.row];
        if (typeof location !== 'undefined') {
           var marcador = mousemarker.setPosition(elevations[e.row].location);
         }
      }
    });

    var arrayElevation = [["","Elevation", { role: 'annotation' }, { role: "style" }]];

    var px = 0;
    for (var i = 0; i < elevations.length; i++) {
      @if($puntosControl[0]['tipo_punto'] == 'KMZ')
      var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','green'];
    arrayElevation.push(arrayPrimero);

      @else
  var lat1 = elevations[i].location.lat();
  var lng1 = elevations[i].location.lng();

  var distanciaElevation = harverstine(lat1,lng1,px);
  var distanciaString = distanciaElevation.toString();
var diferenciaDistancia = distanciaString.split('.');
if (px >= controlPunto.length) {
  px = controlPunto.length - 1;
}

if (diferenciaDistancia[0] == 0) {
  if (controlPunto[px].tipo == 'Punto de control') {
    console.log(lat1);
    var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','blue'];
    arrayElevation.push(arrayPrimero);
  }else{
    var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','red'];
    arrayElevation.push(arrayPrimero);
  }    
  
  px = parseInt(px+1);
  }else{
    var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','green'];
    arrayElevation.push(arrayPrimero);
    
  }
      @endif



  
}



function harverstine(lat1,lon1,px){
  Number.prototype.toRad = function() {
   return this * Math.PI / 180;
}

if (px >= controlPunto.length) {
  px = controlPunto.length - 1;
}

var lat2 = parseFloat(controlPunto[px].lat);
var lon2 = parseFloat(controlPunto[px].lng);

var R = 6371; // km 
//has a problem with the .toRad() method below.
var x1 = lat2-lat1;
var dLat = x1.toRad();  
var x2 = lon2-lon1;
var dLon = x2.toRad();  
var a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
                Math.sin(dLon/2) * Math.sin(dLon/2);  
var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
var d = R * c; 

return d;
}

var data = google.visualization.arrayToDataTable(arrayElevation);
    chart.draw(data, {
      height: 150,
      legend: 'none',
      titleY: 'Elevation(m)'
    });

  }
}

    function redondear(ubicacion){
      var ubicacion = Math.round10(ubicacion,-2);      
      return ubicacion;

    }


    function transformarString(ubicacion){
      var stringLat = ubicacion.toString();
      var arrayLat  = stringLat.split(".");
      var arrayLat =  arrayLat[1].substr(0,3)
      return arrayLat;
    }

@if(count($latLonUbi) > 0)
@if($race->status == 1)


function cluster(map){
  var labelsUbi = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var markersUbi = latLonUbi.map(function(latLonU, i) {
    return new google.maps.Marker({
      position: latLonU,
      label: labelsUbi[i % labelsUbi.length]
    });
  });
  var markerCluster = new MarkerClusterer(map, markersUbi,
    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
}

var latLonUbi = [
@foreach($latLonUbi as $ubi => $lanLon)
{lat: {{ $lanLon[0]['latitud'] }}, lng:{{ $lanLon[0]['longitud'] }} },
@endforeach
];
@endif
@endif
</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>
