@extends('layouts.template')
@section('content')

<div class="alert alert-success alert-dismissible" role="alert" style="display: none" id="mensaje-success">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="alert alert-danger alert-dismissible" role="alert" style="display: none" id="mensaje-error">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>

<div class="my-3 my-md-5">
 <div class="container">
  <div class="page-header">
    <h1 class="page-title">
     Importar participantes
   </h1>
 </div>
 @if (count($errors) > 0)
    <div class="alert alert-danger">
      <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (\Session::has('errores'))
    <div class="alert alert-danger">
      <p>Corrige los siguientes errores:</p>
        <ul>
            @foreach (\Session::get('errores') as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{{  \Session::get('success') }}</li>
        </ul>
    </div>
@endif


@if(\Session::has('danger'))
<div class="alert alert-danger">
        <ul>
            <li>{{  \Session::get('danger') }}</li>
        </ul>
    </div>
@endif
 <div class="col-lg-12">
  <form action="{{url('import/')}}" enctype="multipart/form-data" method="POST">
    @csrf
    <div class="form-group col-md-6">
      <div class="form-label">Importar archivo</div>
      <div class="custom-file">
        <input type="file" class="custom-file-input" name="csv" id="files">
        <label class="custom-file-label">Elegir archivo</label>
      </div>
    </div>

    <div class="card-footer">
      <p>El archivo debe ser separado por coma, sin delimitador de campos de texto y la fila principal debe seguir este estricto orden: num_dorsal,nombre,apellido,direccion,telefono,dni. Para descargar un ejemplo haga <a href="{{ asset('csvejemplo/') }}">click aqui</a>.</p>
      <a href="{{ asset('/carreras/') }}" class="btn btn-danger">Atras</a>
      <input type="hidden" value="{{$carrera['id']}}" name="id_carrera" id="id_carrera" class="text-right">
      <input type="submit" value="Enviar" class="btn btn-primary text-right">

    </div>
  </form>

</div>
</div>

</div>

@endsection