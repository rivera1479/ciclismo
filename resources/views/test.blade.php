
<form action="{{url('cargaCsv/')}}" enctype="multipart/form-data" method="POST">
	@csrf
	<div class="form-group col-md-6">
		<div class="form-label">Importar archivo	

@if (count($errors) > 0)
    <div class="alert alert-danger">
    	<p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (\Session::has('errores'))
    <div class="alert alert-danger">
    	<p>Corrige los siguientes errores:</p>
        <ul>
            @foreach (\Session::get('errores') as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{{  \Session::get('success') }}</li>
        </ul>
    </div>
@endif

{{-- @if (\Session::has('errores')) --}}

@if(\Session::has('danger'))
<div class="alert alert-danger">
        <ul>
            <li>{{  \Session::get('danger') }}</li>
        </ul>
    </div>
@endif
</div>
		<div class="custom-file">
			<input type="file" class="custom-file-input" name="csv" id="files">
			<label class="custom-file-label">Elegir archivo</label>
		</div>
	</div>
	<div class="card-footer">
		<p>El archivo debe ser separado por coma, sin delimitador de campos de texto y la fila principal debe seguir este estricto orden: num_dorsal,nombre,apellido,direccion,telefono,dni. Para descargar un ejemplo haga <a href="{{ asset('csvejemplo/') }}">click aqui</a>.</p>
		<a href="{{ asset('/carreras/') }}" class="btn btn-danger">Atras</a>
		
		<input type="submit" value="Enviar" class="btn btn-primary text-right">

	</div>
</form>
