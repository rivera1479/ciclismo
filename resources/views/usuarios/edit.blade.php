@extends('layouts.template')
@section('content')



 <div class="my-3 my-md-5">
	    <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Editar usuario
              </h1>
            </div>
            
            <div class="col-lg-8">

              <form class="card" method="POST" action="{{route('usuarios.update')}}" enctype="multipart/form-data">
              	 @csrf
                <div class="card-body">
                  <h3 class="card-title">Editar usuario</h3>
                  <div class="row">

                    
                  <input type="hidden" value="{{$usuario['id']}}" name="id">
                      <div id="admin" class="col-md-12">
                        
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="form-label">Nombre de usuario</label>
                            <input type="text" class="form-control" placeholder="Nombre" value="{{ $usuario['nombre'] }}" name="nombre">
                          </div>
                        </div>
                     

                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="form-label">Email</label>
                            <input type="text" class="form-control" placeholder="" value="{{ $usuario['email'] }}" name="email">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="form-label">Password</label>
                            <input type="password" class="form-control passwordedit1" placeholder="contraseña" value="" name="password">
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="form-label">Repetir Password</label>
                            <input type="password" class="form-control passwordedit2" placeholder="contraseña" value="">
                          </div>
                        </div>
                     </div>

                  </div>
                </div>
			</div>



                <div class="card-footer text-right">
                  <a href="{{ url('/usuarios/') }}" class="btn btn-danger">Atras</a>  
                  <button type="submit" class="btn btn-primary btnPasswordEdit">Editar Usuario</button>
                </div>
              </form>
        </div>
    </div>

@endsection
