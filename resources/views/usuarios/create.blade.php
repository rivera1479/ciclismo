@extends('layouts.template')
@section('content')



 <div class="my-3 my-md-5">
	    <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Crear usuario
              </h1>
            </div>
            <div class="col-lg-8">

              <form class="card" method="POST" action="{{route('usuarios.store')}}" enctype="multipart/form-data">
              	 @csrf
                <div class="card-body">
                  <h3 class="card-title">Crear usuario</h3>
                  <div class="row">

                      
                    <div class="form-group">
                        <label class="form-label">Rol</label>
                        <select class="form-control custom-select" id="rol" name="rol" placeholder="hola">
                          <option value="" disabled selected>Seleccione un rol</option>
                          <option value="1">Admin</option>
                        </select>
                    </div>
                    
                    
                    

                      <div id="admin" style="display: none;" class="col-md-12">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="form-label">Nombre de usuario</label>
                            <input type="text" class="form-control" placeholder="Nombre" value="" name="nombre_admin">
                          </div>
                        </div>
                     

                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="form-label">Email</label>
                            <input type="text" class="form-control" placeholder="" value="" name="email">
                          </div>
                        </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-label">Password</label>
                          <input type="password" class="form-control password1" placeholder="password" value="" name="password_user">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-label">Repetir Password </label>
                          <input type="password" class="form-control password2" placeholder="password" value="" name="password_user">
                        </div>
                      </div>
                     </div>
                    <div id="user" style="display: none;" class="col-md-12">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-label">Nombre de usuario</label>
                          <input type="text" class="form-control" placeholder="Nombre" value="" name="nombre_user">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-label">Num dorsal</label>
                          <input type="text" class="form-control" placeholder="" value="" name="num_dorsal">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-label">Password</label>
                          <input type="password" class="form-control" placeholder="password" value="" name="password_user">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-label">Repetir Password </label>
                          <input type="password" class="form-control" placeholder="password" value="" name="password_user">
                        </div>
                      </div>
                    </div>
                    

                  </div>
                </div>
			</div>



                <div class="card-footer text-right">
                  <a href="{{ url('/usuarios/') }}" class="btn btn-danger">Atras</a>  
                  <button type="submit" class="btn btn-primary btnPassword">Crear Usuario</button>
                </div>
              </form>
        </div>
    </div>


    <script>
      $(document).ready(function(){
        $("select[name=rol]").change(function(){
            var rol = $('select[name=rol]').val();
            if (rol == 1) {
              $("#user").css('display','none');
              $("#admin").css('display','block');
            }else{
              $("#admin").css('display','none');
              $("#user").css('display','block');
            }
            
        });
      
      
      });
    </script>

@endsection