@extends('layouts.template')
@section('content')
@include('alerts.messages')


 <div class="my-3 my-md-5">
        <div class="container">
            <div class="page-header">
              <h1 class="page-title">
               Usuarios
              </h1>
            </div>
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Usuarios</h3>
                  </div>
                  <div>
                    <a href="{{ url('/usuarios') }}" class="btn btn-danger">Atras</a>  
                    <a href="{{ asset('usuarios/create/') }}" class=" btn btn-primary">Crear usuarios</a>
                  </div>
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Usuarios</h3>
                  </div>
                  <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap">
                      <thead>
                        <tr>
                          <th class="w-1">Nombre</th>
                          <th>Rol</th>
                          <th>Email</th>                          
                          <th>Numero Dorsal</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($usuarios as $usuario)
                        @php  $rol = $usuario->roles()->first(); @endphp
                        <tr>
                          <td><span class="text-muted">{{ $usuario['nombre'] }}</span></td>
                          <td>{{$rol['description']}}</td>
                          <td>{{ $usuario['email'] }}</td>                          
                          <td>{{ $usuario['num_dorsal'] }}</td>
                          <td class="text-right">
                            <a class="icon" href="{{ url('/usuarios/edit',$usuario['id'])}}"><i class="fe fe-edit"></i></a>
                            <form action="{{ route('usuarios.destroy',$usuario['id'])}}" method="POST">
                              @csrf
                              {{ method_field('DELETE') }}
                              
                              <button class="fa fa-trash icon"></button>
                            </form>
                          </td>

                        </tr>
                        
                        @endforeach

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                  


                </div>
              </div>
    </div>
</div>
    <script></script>
@endsection