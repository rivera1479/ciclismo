<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Seguimiento de corredores | TuPosition</title>
    <meta name="keywords" content="seguimiento,carrera,gps deportivo,cronometraje, tuposition">
    <meta name="description" content="Seguimiento de corredores en las distintas carreras patrocinadas por TuPosition">
    <meta name="DC.title" content="TuPosition">
    <meta property="og:title" content="TuPosition">
    <meta property="og:description" content="Seguimiento de corredores en las distintas carreras patrocinadas por TuPosition">
    <meta property="og:locale" content="es_ES">
    <meta property="og:site_name" content="TuPosition">
    <meta property="og:url" content="https://tuposition.es/">
    <meta property="og:image" content="https://tuposition.es/images/tuposition.jpeg">
    <meta property="og:type" content="website">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta name="distribution" content="all">
    <meta name="robots" content="all">
    <meta name="revisit" content="7 days">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="TuPosition">
    <meta name="twitter:image" content="https://tuposition.es/images/tuposition.jpeg">
    <meta name="twitter:description" content="Seguimiento de corredores en las distintas carreras patrocinadas por TuPosition">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    {{-- {!!Html::style('assets/css/bootstrap.min.css')!!} --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">  
    <script src="{{ asset('/assets/js/vendors/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('/assets/js/require.min.js') }}"></script>
    <script>
        requirejs.config({
            baseUrl: '{{ url('') }}'
        });
    </script>
    <link href="{{ asset('/assets/css/dashboard.css') }}" type="text/css" rel="stylesheet" />
    <script src="{{ asset('/assets/js/dashboard.js') }}"></script>

    <!-- c3.js Charts Plugin -->
    <link href=" {{ asset('/assets/plugins/charts-c3/plugin.css') }}" rel="stylesheet" />
    <script src=" {{ asset('/assets/plugins/charts-c3/plugin.js') }}"></script>
    <!-- Google Maps Plugin -->
    <link href=" {{ asset('/assets/plugins/maps-google/plugin.css') }}" rel="stylesheet" />
    <script src=" {{ asset('/assets/plugins/maps-google/plugin.js') }}"></script>
    <!-- Input Mask Plugin -->
    <script src=" {{ asset('/assets/plugins/input-mask/plugin.js') }}"></script>
    <script src=" {{ asset('/assets/plugins/jssor.slider.min.js') }}"></script>
    <link href="{{ asset('/css/app.css') }}" type="text/css" rel="stylesheet" />

</head>
<body class="">
    <input type="hidden" id="url_base" value="{{ url('') }}">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="page">
        <div class="page-main">

            <div class="container">
                <div class="row align-items-center">



                    <div id="centrador-img-logo">
                        <img id="imagen-logo" src="{{ asset('/images/logo.jpg') }}"  alt="Logo">
                    </div>

                    <div class="col-md-12 text-center">
                        <h1 class="titulo" style="color:#2d2d2d">SEGUIMIENTO DE CARRERAS</h1>
                    </div>
                    
                </div>
            </div>

            <div class="container home">
                <nav class="navbar navbar-expand-sm navbar-light row"  style="background-color: #ef3f29;height: 3rem;padding: 0">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#opciones">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- enlaces -->
                    <div class="menu collapse navbar-collapse" id="opciones">
                        <ul class="nav justify-content-center" >
                            <li class="nav-item">
                                <a href="{{url('/')}}" class="nav-link menu-nav" style="color: white"> <strong>INICIO</strong></a>
                            </li>
                            @guest
                            @else
                            @if(Auth::user()->roles()->first()->id == 1)
                            <li class="nav-item">
                                <a href="{{url('/carreras')}}" class="nav-link menu-nav" style="color: white"> <strong>ADMINISTRACION</strong></a>
                            </li>
                            @endif
                            @endguest
                        </ul>
                    </div>
                </nav>

                <h1 class="titulo" style="margin-left: 50px;">
                    LISTADO DE CARRERAS
                </h1>                       
                <div class="card" id="contenedor-carreras" style="margin-bottom: auto;">
                  <div class="row justify-content-center">
                   <div class="card-body row">                    
                        @if($carreras==null)
                        <h1>No hay carreras creadas</h1>
                        @else
                        @foreach($carreras as $carrera)
                        @if($carrera->visibility == 0 || $carrera->visibility == 1 )
                        <div class="col-sm col-lg-3">
                            <div class="race">
                                <div class="img" style="background: url({{asset('/img/'.$carrera['img_cabecera'])}}) center no-repeat;background-size: cover">
                                </div>
                                <div class="descripcion">
                                    <time style="font-family: 'Arial';" >{{ $carrera->fecha}}</time>
                                    <h3 class="nombre-carrera">{{ $carrera->nombre }}</h3>
                                    <p>
                                        <span class="km" style="font-family: 'Arial';">KM: {{ $carrera->km_totales }}</span>
                                    </p>

                                    <a href="{{ route('carrera', [$carrera->url]) }}" class="btn btn-seguidor btn-block">SEGUIR AL CORREDOR</a>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                        <center>  {{ $carreras->links() }}</center>
                </div>
            </div>
        </div>
    </div>




    <footer>
        <div class="design">
            <p>Diseño y Desarrollo por <a href="https://www.audituxinformatica.com/" target="blank"><img src="{{ asset('/images/auditux-logo.png') }}" alt="Auditux Informatica" style=" vertical-align: middle;"></a></p>
        </div>
    </footer>
    <script src="{{ asset('js/Template.js') }}"></script>
</body>
</html>