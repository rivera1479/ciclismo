@extends('layouts.templatedetalles')
@section('content')
@php
$participantes= $race->participantes()->get();
$patrocinadores= $race->patrocinadores()->get();

@endphp

<div class="card pagina-detalles">   

    <div class="row row-cards row-deck" >
      <div class="col-12 text-center">
        <h1 class="page-title titulo" style="line-height: 70px">
                  SEGUIMIENTO DE CARRERA
              </h1>        
      </div>
      @isset($participante)

      <div class="col-12">
        <div class="row" style="padding: 0 20px;">
          <div class="col-md-6 col-lg-4 col-xl-3">
            <div class="img-dorsal" >
              <div class="texto-encima-centrado">{{$participante['num_dorsal']}}</div>
              <div class="texto-arriba">{{$participante['nombre']}} {{$participante['apellido']}}</div>
            </div>
          </div>
          <div class="col-md-6 col-lg-8 col-xl-9 text-center">
            <div class="card">
              <div style="background:url({{asset('/img/'.$race['img_cabecera'])}}) center no-repeat;background-size:cover;padding: 0;" class="cabecera">
              </div>
            </div>
          </div>
        </div>
      </div>
 @else
      <div class="col-12 text-center">
        <div class="card">
          <div style="background:url({{asset('/img/'.$race['img_cabecera'])}}) center no-repeat;background-size:cover;padding: 0;" class="cabecera">
          </div>
        </div>
      </div>
 @endisset

    @if(!isset($participante))


    @include('carreras.mapas.withoutparticipants')
    @else
    @isset($ubicacion)
      <div class="col-12 text-center">
        <div class="card">
          <div class="card-header" style="display: block;border:0">
            <h3 class="card-title titulo">RUTA DE CARRERA</h3>
          </div>

          <div class="card-body">
            <div id="map" class="map"></div>
            <div id="elevation_chart" class="c3"></div>
            <div id="curve_chart"></div>

          </div>
        </div>
      </div>
{{-- Crear mapa en el resultado de la busqueda --}}
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
  google.charts.load("current", {packages:['corechart']});
  

  var map;
  var mousemarker;
  var tiemposPuntos = [];
  var icons;
  var carrera_id;
  var controlPunto = [];
  function initMap() {

      @foreach($puntosControl as $punto)
    @if($punto['tipo_punto'] == 'Punto de control' or $punto['tipo_punto'] == 'Puerto')
      var latPunto = '{{$punto['latitud']}}';
      var lngPunto = '{{$punto['longitud']}}';
      var tipo_punto  = '{{$punto['tipo_punto']}}';

      controlPunto.push({
        lat:latPunto,
        lng:lngPunto,
        tipo: tipo_punto
      });
    @endif

  @endforeach
    carrera_id = "{{$race['id']}}";
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
    var elevator = new google.maps.ElevationService;

    @if($puntosControl[0]['tipo_punto'] == 'KMZ')
    var rutaCoordinates =[
    @foreach($puntosControl as $punto)
    {lat:{{$punto['latitud']}},lng:{{$punto['longitud']}}},
    @endforeach
    ];

    var rutaMedia = parseInt(rutaCoordinates.length / 2);
    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var labelIndex = 0;

    var map = new google.maps.Map(document.getElementById('map'),{
      zoom: 8,
      center: rutaCoordinates[rutaMedia]
    });

    var rutaPath = new google.maps.Polyline({
      path: rutaCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });


    rutaPath.setMap(map);
    zoomToObject(rutaPath);
        var participantes = [];
    var arrayhu = [];
    @foreach($ubicacion as $datos)
    var participante =  new google.maps.LatLng(parseFloat({{$datos['latitud']}}),parseFloat({{$datos['longitud']}}));;
    participantes.push(participante);
    var hu = "{{$datos['fecha_competencia']}}";
    arrayhu.push(hu);
    @endforeach

    var rutaCoordinates2 =[
    @foreach($ubicacion as $punto)
    {lat:{{$punto['latitud']}},lng:{{$punto['longitud']}}},
    @endforeach
    ];
        var rutaPath2 = new google.maps.Polyline({
      path: rutaCoordinates2,
      geodesic: true,
      strokeColor: '#141be2',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

        rutaPath2.setMap(map);
    zoomToObject(rutaPath2);

    function zoomToObject(obj){
      var bounds = new google.maps.LatLngBounds();
      var points = obj.getPath().getArray();
      for (var n = 0; n < points.length ; n++){
        bounds.extend(points[n]);
      }
      map.fitBounds(bounds);
    }



    var icons = {
      jugador: new google.maps.MarkerImage(
        '{{asset('images/participante.png')}}', 
        new google.maps.Size(44, 32), 
        new google.maps.Point(0, 0), 
        new google.maps.Point(22, 32) 
        )

    };

/*    for (var i = 0; i < participantes.length; i++) {
      makeMarkerParticipante(participantes[i], icons.jugador, arrayhu[i]);
    }*/
    elevator.getElevationAlongPath({
          'path': rutaCoordinates,
          'samples': 256
        }, plotElevation);



    @else


    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var labelIndex = 0;
    var map = new google.maps.Map(document.getElementById('map'),{
      zoom: 12,
      center: {lat: 40.94082477819901, lng: -3.995340561054263},
      mapTypeId: 'terrain'
    });
    directionsDisplay.setMap(map);
    // Create an ElevationService.
    
    // Draw the path, using the Visualization API and the Elevation service. 
    
    crearRuta();

    function crearRuta() {
      var icons = { 
        start: new google.maps.MarkerImage(
        '{{asset('images/inicio.png')}}',
        new google.maps.Size(44, 32),
        new google.maps.Point(0, 0),
        new google.maps.Point(22, 32) 
        ), 
        end: new google.maps.MarkerImage(
        '{{asset('images/finish.png')}}',
        new google.maps.Size(44, 32), 
        new google.maps.Point(0, 0), 
        new google.maps.Point(22, 32) 
        ),
        jugador: new google.maps.MarkerImage(
        '{{asset('images/participante.png')}}', 
        new google.maps.Size(44, 32), 
        new google.maps.Point(0, 0), 
        new google.maps.Point(22, 32) 
        ),
        puerto: new google.maps.MarkerImage(
        '{{asset('images/marina-2.png')}}',
        new google.maps.Size(44, 32),
        new google.maps.Point(0, 0), 
        new google.maps.Point(22, 32) 
        ),
        puntoControl: new google.maps.MarkerImage(
        '{{asset('images/clock.png')}}',  
        new google.maps.Size(44, 32),  
        new google.maps.Point(0, 0),  
        new google.maps.Point(22, 32) 
        ),
      };
      var ori = "{{$origen}}";
      var des = "{{$destino}}";
      var origenArray = ori.split(",");
      var destinoArray = des.split(",");
      var waypoints = [];
      var waypointsCompleto = [];
      var participantes = [];
      var arrayhu = [];
      var origen = new google.maps.LatLng(parseFloat(origenArray[0]),parseFloat(origenArray[1]));
      var destino =  new google.maps.LatLng(parseFloat(destinoArray[0]),parseFloat(destinoArray[1]));
    
      @foreach($arrayLatLon as $key => $value)
      var way = new google.maps.LatLng(parseFloat({{$value['lat']}}),parseFloat({{$value['lon']}}));
      var tipo_punto = "{{$value['tipo_punto']}}";
      var titulo = "{{$value['titulo']}}";
      var km = "{{$value['km']}}";
      var tiempo = "{{$value['tiempo']}}";

      waypoints.push({
        location:way,
        stopover: true});
      
      waypointsCompleto.push({
        tipo_punto : tipo_punto,
        titulo: titulo,
        km: km,
        tiempo:tiempo
      });
      @endforeach
      @foreach($ubicacion as $datos)
      var participante =  new google.maps.LatLng(parseFloat({{$datos['latitud']}}),parseFloat({{$datos['longitud']}}));;
      participantes.push(participante);
      var hu = "{{$datos['fecha_competencia']}}";
      arrayhu.push(hu);

      @endforeach
      

      directionsService.route({
            origin:  origen ,
            destination: destino,
            waypoints:  waypoints,
            travelMode: 'BICYCLING'
          }, function(response, status){
            if(response, status){              
              directionsDisplay.setDirections(response);

              elevator.getElevationAlongPath({
                path: response.routes[0].overview_path,
                samples: 256
              }, plotElevation);
              
              var leg = response.routes[ 0 ].legs[ 0 ]; 
              makeMarker(leg.start_location, icons.start, "Inicio de punto");
              @empty($tiempoFinal)
              var tiempoFinal = "";
              @else
              var tiempoFinal = "{{ $tiempoFinal['tiempo'] }}";
              @endempty
              var end_km = "{{ $race['km_totales'] }}";

              if (tiempoFinal != '') {
                makeMarker(destino, icons.end, ' Final de punto',end_km +" KM",tiempoFinal);
              }else{
                makeMarker(destino, icons.end, 'Final de punto',end_km+" KM");
              }
              var km_total = 0;
              for (var i = 0; i < waypoints.length; i++) {
                var km = parseInt(waypointsCompleto[i]['km']);
                km_total = km_total + km;
                
                if (waypointsCompleto[i]['tipo_punto'] == 'Puerto') {
                  makeMarker(waypoints[i]['location'], icons.puerto, waypointsCompleto[i]['titulo'],km_total+" KM");
                }
                if (waypointsCompleto[i]['tipo_punto'] == 'Punto de control') {
                  makeMarker(waypoints[i]['location'], icons.puntoControl, waypointsCompleto[i]['titulo'],km_total+" KM",waypointsCompleto[i]['tiempo']);
                }if (waypointsCompleto[i]['tipo_punto'] == 'Punto normal') {
                } 
              }
              for (var i = 0; i < participantes.length; i++) {
                makeMarkerParticipante(participantes[i], icons.jugador, arrayhu[i]);
              }
            }
            else{
              window.alert('Directions request failed due to ' + status);
            }
      });

      function makeMarker(position, icon, title,km = "",tiempo = "") { 
        var markerInfo=  new google.maps.Marker({
          position: position, 
          map: map, 
          icon: icon, 
          title: title
        });
        var stepDisplay = new google.maps.InfoWindow;
        google.maps.event.addListener(markerInfo, 'click', function(){
          if (tiempo == 0 || tiempo == "" ) {
            var text = "<p> Distancia de punto: "+km+"</p>" ;
          }else{
            var text = "<p> Distancia de punto: "+km+"</p> <p> Tiempo: "+tiempo+"</p>" ;
          }
          stepDisplay.setContent(text);
          stepDisplay.open(map, markerInfo);
        });
      }

    }

    
    @endif

    function plotElevation(elevations, status) {
      var chartDiv = document.getElementById('elevation_chart');
      if (status !== 'OK') {
        chartDiv.innerHTML = 'Cannot show elevation: request failed because '+
        status;
        return;
      }
      var chart = new google.visualization.ColumnChart(chartDiv);
      google.visualization.events.addListener(chart, 'onmouseover', verInfoOver);
      google.visualization.events.addListener(chart, 'onmouseout', verInfoOut);

      google.visualization.events.addListener(chart, 'onmouseover', function(e){
        if (mousemarker == null) {
          var latLngEle = elevations[e.row].location;
          var stringlatLngEle = JSON.stringify(latLngEle);
          var arraylatLngEle  = stringlatLngEle.split(":");
          var lat = arraylatLngEle[1].split(",");
          var lng = arraylatLngEle[2].replace("}","");
          mousemarker = new google.maps.Marker({
            position: elevations[e.row].location,
            map: map,
            icon: "{{asset('images/iconelevation.png')}}"
          });
        } else {
          var location = elevations[e.row];
          if (typeof location !== 'undefined') {
            mousemarker.setPosition(elevations[e.row].location);
          }
        }
      });

      var arrayElevation = [["","Elevation", { role: 'annotation' }, { role: "style" }]];
      var distancia;
      var di;
      var px = 0;
      for (var i = 0; i < elevations.length; i++) {
         @if($puntosControl[0]['tipo_punto'] == 'KMZ')
         var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','green'];
    arrayElevation.push(arrayPrimero);

         @else
  var lat1 = elevations[i].location.lat();
  var lng1 = elevations[i].location.lng();

  var distanciaElevation = harverstine(lat1,lng1,px);
  var distanciaString = distanciaElevation.toString();
var diferenciaDistancia = distanciaString.split('.');

if (px >= controlPunto.length) {
  px = controlPunto.length - 1;
}

if (diferenciaDistancia[0] == 0) {
  if (controlPunto[px].tipo == 'Punto de control') {
    console.log(lat1);
    var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','blue'];
    arrayElevation.push(arrayPrimero);
  }else{
    var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','red'];
    arrayElevation.push(arrayPrimero);
  }    
  
  px = parseInt(px+1);
  }else{
    var ele = Math.round(elevations[i].elevation);
    var arrayPrimero = ["",ele,'','green'];
    arrayElevation.push(arrayPrimero);
  }
         @endif




  
      }

function harverstine(lat1,lon1,px){
  Number.prototype.toRad = function() {
   return this * Math.PI / 180;
}

if (px >= controlPunto.length) {
  px = controlPunto.length - 1;
}

var lat2 = parseFloat(controlPunto[px].lat);
var lon2 = parseFloat(controlPunto[px].lng);

var R = 6371; // km 
//has a problem with the .toRad() method below.
var x1 = lat2-lat1;
var dLat = x1.toRad();  
var x2 = lon2-lon1;
var dLon = x2.toRad();  
var a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
                Math.sin(dLon/2) * Math.sin(dLon/2);  
var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
var d = R * c; 

return d;
}

      var data = google.visualization.arrayToDataTable(arrayElevation);

      function verInfoOver(e){
        chart.setSelection([e]);
      }
      function verInfoOut(e){
        chart.setSelection([{'row': null, 'column': null}]);
      }
      chart.draw(data, {
        height: 150,
        legend: 'none',
        titleY: 'Elevation (m)',
        title: 'Elevacion de ruta'
      });
      var dataTimeLine = new google.visualization.DataTable();
      dataTimeLine.addColumn('number','KM');
      dataTimeLine.addColumn('timeofday','Tiempo');
      arrayKmTiempo = [0,[0,0,0]];
      var arrayTiempoLine = [];
      arrayTiempoLine.push(arrayKmTiempo);
      @foreach($tiemposGrafica as $tiempo)
      var tiempo = "{{$tiempo['tiempo']}}";
      var h = tiempo.split(':');
      var hora = parseInt(h[0]);
      var min = parseInt(h[1]);
      var sec = parseInt(h[2]);
      var km = "{{$tiempo['distancia']}}";
      km = parseInt(km);
      var arrayKmTiempo = [km,[hora,min,sec]];
      arrayTiempoLine.push(arrayKmTiempo);
      @endforeach
      dataTimeLine.addRows(arrayTiempoLine);
      @empty($tiemposGrafica)
      @else
      var options = {
        point: {visible: true},
        title:'Gráfica de tiempo',
        vAxis: {title: 'Tiempo'},
        legend: { position: 'bottom' }
      };

      var chartLine = new google.visualization.LineChart(document.getElementById('curve_chart'));
      chartLine.draw(dataTimeLine, options);
      @endempty
    }
      function makeMarkerParticipante(position,icon,fecha) { 
        var markerInfo=  new google.maps.Marker({
          position: position, 
          map: map, 
          icon: icon, 
          title: fecha
        });
        var stepDisplay = new google.maps.InfoWindow;
        google.maps.event.addListener(markerInfo, 'click', function(){
          var hora = fecha.split(' ');
          var text = "<p> Participante <br>"+hora[1]+"</p>";
          stepDisplay.setContent(text);
          stepDisplay.open(map, markerInfo);
        });
      }
  }


</script>


    @endisset
    @empty($tiempos)
    <div class="col-12 text-center">
      <h1>No hay tiempos registrados</h1>
    </div>
    
    @else

      {{-- Tabla de tiempos --}}
      <div class="col-12">
        <div class="card">
          <div class="table-responsive" style="padding-left: 25px;padding-right: 25px;padding-bottom: 10px;">
            <table class="table card-table table-striped table-vcenter" style="border: 1px solid #dee2e6;padding: 15px;">
              <thead>
                <tr>
                  <th>Punto de control</th>
                  <th>Distancia</th>
                  <th class="text-center">Tiempo</th>
                </tr>
              </thead>
              <tbody>
                @foreach($tiempos as $tiempo)
                <tr>
                  <td>{{$tiempo['titulo']}}</td>
                  <td>KM: {{$tiempo['distancia']}}</td>
                  <td class="text-center"><i class="fa fa-clock-o text-muted"></i> {{$tiempo['tiempo']}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @endempty
@endif
      <div class="logos col-12">
        <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:100px;overflow:hidden;visibility:hidden;">
          <!-- Loading Screen -->
          <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
          </div>
          <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:100px;overflow:hidden;">
            @foreach($patrocinadores as $patrocinador) 
            <div>
              <a @php echo ($patrocinador['url']!='')? 'target="_blank" href="'.$patrocinador['url'].'"': 'href="#"'; @endphp>
                <img data-u="image" src="{{asset('/img/'.$patrocinador['img'] )}}" alt="{{ $patrocinador['nombre'] }}"/>
              </a>
            </div>
            @endforeach
            @foreach($patrocinadores as $patrocinador)
            <div>
              <a @php echo ($patrocinador['url']!='')? 'target="_blank" href="'.$patrocinador['url'].'"': 'href="#"'; @endphp>
                <img data-u="image" src="{{asset('/img/'.$patrocinador['img'] )}}" alt="{{ $patrocinador['nombre'] }}"/>
              </a>
            </div>
            @endforeach
          </div>
        </div>
      </div>


  
  
</div>
  


<script src="{{ asset('/js/jssor.slider.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {
            var jssor_1_options = {
              $AutoPlay: 1,
              $Idle: 0,
              $SlideDuration: 5000,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Align: 0
            };
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            /*#region responsive code begin*/
            var MAX_WIDTH = 980;
            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;
                if (containerWidth) {
                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <script type="text/javascript">jssor_1_slider_init();</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9y3PjQzU5P5cFGGDzD8dqZi_6WfGvtfQ&callback=initMap"
  type="text/javascript"></script>
@endsection