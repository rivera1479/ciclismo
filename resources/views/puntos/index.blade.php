@extends('layouts.template')
@section('content')
<div class="mensaje"></div>

<div class="row row-cards row-deck">
  <div class="col-lg-6">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{$carreras[0]['nombre']}}</h3>
      </div>
      <div class="card card-aside">     
        <input type="hidden" value="{{$tiempos}}" name="tiempos">
        <div class="form">
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="participante_id" value="{{$participante->id}}">
          @php $km = 0; @endphp
          @foreach($puntos as $punto)
            @php $km = $km + $punto['km']; @endphp
          @if($punto['tipo_punto'] == 'Punto de control')
          
          <div class="form-group vueltas">
            <div>
              <input type="hidden" name="puntos_control_id" value="{{$punto['id']}}">
            </div>
            <label for="punto{{$punto['id']}}">{{$punto['titulo']}}</label>
            <input type="datetime" name="tiempo{{$punto['id']}}" class="tiempo{{$punto['id']}} tiempo" id="tiempo{{$punto['id']}}" disabled>
            <input type="button" class="btn btn-primary anadir" value="Agregar punto" id="button{{$punto['id']}}" disabled>
            <input type="hidden" value="{{ $km }}" name="km">
            <input type="hidden" value="{{ $punto['titulo'] }}" name="nombre_control">
          </div>
          @endif
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Participante</h3>
      </div>
      <div class="card card-aside">
        <div class="card-body d-flex flex-column">
          <h4><a href="#">Nombres: <p>{{$participante['nombre']}} {{$participante['apellido']}} </p></a></h4>
          <div class="d-flex align-items-center pt-5 mt-auto">
            <div>
              <div class="text-default"> Numero dorsal: <p>{{$participante['num_dorsal']}}</p></div>
              <small class="d-block text-muted"> DNI: <p>{{$participante['dni']}}</p></small>
            </div>

          </div>
        </div>
      </div>     
    </div>
  </div>
</div>






<script>
  $(document).ready(function(){
    $(".anadir").each(function(el){
      $(this).on("click",anadirPunto);
    });


    var tiempo = {!! $tiempos->toJson() !!};



    var inputs= [];
    var inputTime = $(".tiempo").each(function(){
      var idTiempo = $(this).attr('id');
      inputs.push(idTiempo);
    });

    for (var i = 0; i < tiempo.length; i++) {
     var time = tiempo[i]['tiempo'];
     $('#'+inputs[i]).val(time);
     $('#'+inputs[i]).prop('disabled',true);
     $('#'+inputs[i]).siblings('input').prop('disabled',true);
   }

   if (tiempo.length > 0){
    $(".tiempo").each(function(){
      var valor = $(this).val();
      if( valor.length <= 0){
        $(this).prop('disabled',false);
        $(this).siblings('input').prop('disabled',false);
      }else{
        $(this).prop('disabled',true);
      }

    });
  }else{
    $(".tiempo:eq(0)").prop('disabled',false);
    $(".anadir:eq(0)").prop('disabled',false);
  }
});


  function anadirPunto(){

    var tiempo = $(this).siblings('input').val();
    var puntoControl = $(this).siblings('div').children('input').val();
    var km = $(this).next().val();
    var nombre_control = $(this).next().next().val();

    var classInputActual = $(this).siblings('input').attr('id');
    var buttonActual = $(this).attr('id');

    var classInputNext = $(this).parent('div').next().children('input[type="datetime"]').attr('id');
    var buttonNext = $(this).parent('div').next().children('input[type="button"]').attr('id');
  
console.log(tiempo);
    $.ajax({
      url:$('#url_base').val()+'/puntos/agregar',
      type:'POST',
      data:{
        '_token':$('input[name=_token]').val(),
        'tiempo':tiempo,
        'puntos_control_id':puntoControl,
        'participante_id':$('input[name=participante_id]').val(),
        'km':km,
        'nombre_control':nombre_control,
      },
      success: function(data)
      {
        if((data.success==true))
        {
          $(".mensaje").show();
          $('.mensaje').removeClass('card-alert alert alert-danger mb-0').addClass('card-alert alert alert-success mb-0');
          $('.mensaje').empty().append(data.mensaje);
          $(".mensaje").hide(3000);
          $("#"+classInputNext).prop('disabled',false);
          $("#"+buttonNext).prop('disabled',false);
          $("#"+classInputActual).prop('disabled',true);
          $("#"+buttonActual).prop('disabled',true);

        }
        else{
         var mensaje='';
         for(var i=0;i<data.errors.length;i++)
         {
          mensaje+=data.errors[i]+'<br>';
        }
        $(".mensaje").show();
        $('.mensaje').removeClass('card-alert alert alert-success mb-0').addClass('card-alert alert alert-danger mb-0');
        $('.mensaje').empty().append(mensaje);
        $(".mensaje").hide(3000);
      }
    }
  });
  }


</script>

@endsection