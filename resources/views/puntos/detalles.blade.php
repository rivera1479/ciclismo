@extends('layouts.template')
@section('content')

<div class="row row-cards row-deck">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Puntos de participante</h3>
      </div>
      @if($puntos->isEmpty())
      <h1>No hay tiempos registrados</h1>
      @else
      <div class="col-sm-6 col-lg-4">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Tiempos</h4>
          </div>
          <table class="table card-table">
            @foreach($puntos as $punto)
            <tr>
              <td width="1"><i class="fa fa-clock-o text-muted"></i></td>
              <td>{{ $punto['tiempo'] }}</td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
@endif
    </div>
  </div>
</div>
@endsection