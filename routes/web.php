<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');





Route::group(['middleware' => 'auth'], function(){
	//Crud de carreras
	Route::get('carreras','CarreraController@index')->name('carreras.index');
	Route::get('carreras/create','CarreraController@create')->name('carreras.create');
	Route::post('carreras/store','CarreraController@store')->name('carreras.store');
	Route::get('carreras/edit/{id}','CarreraController@edit')->name('carreras.edit');
	Route::post('carreras/update','CarreraController@update')->name('carreras.update');
	Route::delete('carreras/destroy/{id}','CarreraController@destroy')->name('carreras.destroy');

	//Puntos de cada carrera
	Route::get('punto/{id}','PuntosController@index');
	Route::get('punto/create/{id}','PuntosController@create');
	Route::get('punto/createkmz/{id}','PuntosController@createkmz');
	Route::post('punto/store','PuntosController@store');
	Route::post('punto/storekmz/{carrera_id}','PuntosController@storekmz')->name('routekmz.store');
	Route::get('punto/edit/{id}','PuntosController@edit');
	Route::post('punto/update','PuntosController@update');
	Route::delete('punto/destroy/{id}','PuntosController@destroy')->name('punto.destroy');
	Route::get('punto/cargaform/{id}','PuntosController@cargarCsv');
	Route::post('guardarCsvTiempos','PuntosController@guardarCsvTiempo');

	//Patrocinadores
	Route::get('patrocinadores/{id}','PatrocinadoresController@index');
	Route::get('patrocinadores/create/{id}','PatrocinadoresController@create');
	Route::post('patrocinadores/store','PatrocinadoresController@store')->name('patrocinadores.store');
	Route::get('patrocinadores/edit/{id}','PatrocinadoresController@edit');
	Route::post('patrocinadores/update','PatrocinadoresController@update')->name('patrocinadores.update');
	Route::delete('patrocinadores/destroy/{id}','PatrocinadoresController@destroy')->name('patrocinadores.destroy');

	//usuarios

	Route::get('usuarios','UsuarioController@index')->name('usuarios.index');
	Route::get('usuarios/create','UsuarioController@create')->name('usuarios.create');
	Route::post('usuarios/store','UsuarioController@store')->name('usuarios.store');
	Route::get('usuarios/edit/{id}','UsuarioController@edit')->name('usuarios.edit');
	Route::post('usuarios/update','UsuarioController@update')->name('usuarios.update');
	Route::delete('usuarios/destroy/{id}','UsuarioController@destroy')->name('usuarios.destroy');

	//agregar ciclistas a carrera manualmente
	Route::get('ciclistas/{id}','ParticipantesController@index')->name('ciclistas.index');
	Route::get('ciclistas/create/{id}', 'ParticipantesController@create')->name('ciclistas.create');
	Route::get('ciclistas/edit/{id}', 'ParticipantesController@edit')->name('ciclistas.edit');
	Route::post('ciclistas/update', 'ParticipantesController@update')->name('ciclistas.update');
	Route::post('ciclistas/store', 'ParticipantesController@store')->name('ciclistas.store');
	Route::delete('ciclistas/destroy/{id}', 'ParticipantesController@destroy')->name('ciclistas.destroy');
	Route::get('ciclistas/buscar', 'ParticipantesController@buscar')->name('ciclistas.buscar');
	Route::get('ciclistas/show/{id}', 'ParticipantesController@show')->name('ciclistas.show');
	Route::post('ciclistas/reiniciarHistorico','ParticipantesController@borrarUbicacion')->name('ciclistas.borrarHistorico');

	//Csv participantes
	Route::get('import/form/{id}','CarreraController@formimport')->name('formimport');
	Route::post('import/','CsvController@import')->name('import');
	Route::get('csvejemplo/','CsvController@descargarEjemplo');

	Route::post('guardar','CarreraController@guardar');
	Route::post('export','CarreraController@export');
	Route::get('map','CarreraController@map');
	Route::post('distancia','CarreraController@distancia');
	
});

Route::get('carreras/{slug}',['as'=> 'carrera','uses'=> 'CarreraController@show']);

Auth::routes();

Route::get('participantes','CarreraController@puntosIndex')->name('puntos');
Route::get('carreras/{slug}/{num}/antes','CarreraController@buscarParticipante');
Route::post('buscar/participante','CarreraController@buscar');
Route::get('carreras/{slug}/{num}','CarreraController@buscarParticipante');
Route::post('puntos/agregar','CarreraController@anadirPunto')->name('anadirpunto');
Route::get('tiempos/{id}',[
	'as'=> 'tiempos',
	'uses' => 'CarreraController@tiempos']);


Route::post('guardarcvs','CarreraController@guardarCvs');
Route::post('leercsv','CarreraController@leercsv');

Route::post('status/{status}','CarreraController@status');


Route::post('importExcel', 'HomeController@importExcel');