<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', 'Api\AuthController@login');
Route::get('carreras', 'Api\CarreraController@index');
Route::post('logincarrera', 'Api\CarreraController@ingresarCarrera');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('profile', 'Api\AuthController@profile');
    Route::post('ubicacion','Api\UbicacionController@store');
});

Route::post('logout','Api\AuthController@logoutApi');

/*LIMPIAR LA CACHE DEL SISTEMA*/
Route::group(['prefix' => 'artisan'], function () {
	Route::get('cache_clear', function () {
	    Artisan::call('cache:clear');
	    return "Cache General Limpiada";
	});

	Route::get('config_cache', function () {
	    Artisan::call('config:cache');
	    return "Cache de Configuracion Limpiada";
	});

	Route::get('migrate', function () {
	    Artisan::call('migrate');
	    return "Migraciones ejecutadas";
	});

});